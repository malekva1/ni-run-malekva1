if [ "$#" -lt 2 ];
then
    echo "Usage:"
    echo " - "$0" run source.fml"
    echo " - "$0" compile source.fml bytecode.bc"
    echo " - "$0" execute bytecode.bc"
    exit 1
fi

ABSOLUTE_PATH="$(realpath "$0")"
SCRIPT_PATH="$(dirname "$ABSOLUTE_PATH")"

if [ "$1" == "execute" ]
then
    shift
    "$SCRIPT_PATH"/hw02/runtime "$@"
    exit 0
fi


if [ "$1" == "compile" ]
then
    if [ "$#" -lt 3 ];
    then
        echo "Usage:"
        echo " - "$0" compile source.fml bytecode.bc"
        exit 1
    fi

    source="$2"
    bytecode="$3"
    json=$(mktemp)

    "$SCRIPT_PATH"/reference.fml parse "$source" --format=json -o "$json"
    "$SCRIPT_PATH"/hw03/bccompiler "$json" "$bytecode"

    exit 0
fi

if [ "$1" == "run" ]
then
    skip_next="false"
    have_source="false"
    declare -a runtime_args

    for arg in "$@"
    do 
        if [ "$skip_next" == "true" ]
        then 
            runtime_args+=" $arg"
            skip_next="false"
            continue
        fi

        if [ "$arg" == "$0" -o "$arg" == "$1" ]
        then 
            continue
        fi

        if [ "$arg" == "--heap-size" -o "$arg" == "--heap-log" ]
        then 
            runtime_args+=" $arg"
            skip_next="true"
            continue
        fi

        if [ "$have_source" == "false" ]
        then
            have_source="true"
            source="$arg"
            continue
        fi

        runtime_args+=" $arg"
    done



    json=$(mktemp)
    bytecode=$(mktemp)

    # set -o xtrace

    "$SCRIPT_PATH"/reference.fml parse "$source" --format=json -o "$json"
    "$SCRIPT_PATH"/hw03/bccompiler "$json" "$bytecode"
    "$SCRIPT_PATH"/hw02/runtime "$bytecode" ${runtime_args[@]}

    exit 0
fi

>&2 echo "Unsuported command $1"
exit 1

