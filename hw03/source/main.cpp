#include <iostream>
#include <string>
#include <sstream>

#include <fstream>

using namespace std;

#include "ast.h"
#include "astbuilder.h"
#include "astprinter.h"
#include "bccompiler.h"

string readInput(int argc, char ** argv)
{
    ostringstream inputStream;
    if (argc > 1)
    {
        ifstream t(argv[1]);
        inputStream << t.rdbuf();
    }
    else
    {
        inputStream << cin.rdbuf();
    }
    return inputStream.str();
}

int main(int argc, char ** argv)
{
    string json = readInput(argc, argv);

    Ast * ast = AstBuilder::build(json);

    string outputfile = argc > 2 ? argv[2] : "output.bc";

    return BcCompiler::compile(ast, outputfile);
}

