#include "localenviroment.h"

Program::LocalFrameIndex LocalEnviroment::addVariable(const std::string & name)
{
    variableStack.back().insert(make_pair(name, lastIndex));
    return lastIndex++;
}

Program::LocalFrameIndex LocalEnviroment::findVariable(const std::string & name) const
{
    for (auto it = variableStack.rbegin(); it != variableStack.rend(); ++it)
    {
        if (it->find(name) == it->end())
        {
            continue;
        }
        return it->at(name);
    }

    throw VariableDoesntExistException();
}

void LocalEnviroment::addLevel()
{
    variableStack.push_back(Level());
}

void LocalEnviroment::removeLevel()
{
    variableStack.pop_back();
}

Program::LocalFrameIndex LocalEnviroment::size() const
{
    return lastIndex;
}
