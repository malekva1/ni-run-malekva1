#ifndef ASTPRINTER_H
#define ASTPRINTER_H

#include "astvisitor.h"

class AstPrinter : protected AstVisitor
{
public:
    static void print(Ast * program);

    void visit(Integer * node);
    void visit(Boolean * node);
    void visit(Null * node);
    void visit(Variable * node);
    void visit(AccessVariable * node);
    void visit(AssignVariable * node);
    void visit(Function * node);
    void visit(CallFunction * node);
    void visit(Print * node);
    void visit(Block * node);
    void visit(Top * node);
    void visit(Loop * node);
    void visit(Conditional * node);

    void visit(Object * node);
    void visit(Array * node);
    void visit(AssignField * node);
    void visit(AssignArray * node);
    void visit(AccessField * node);
    void visit(AccessArray * node);
    void visit(CallMethod * node);

private:
    AstPrinter();
    void printSpaces();
    int spaces;


};

#endif // ASTPRINTER_H
