#ifndef ASTBUILDER_H
#define ASTBUILDER_H

class Ast;

#include "json.hpp"
#include <vector>

class AstBuilder
{
public:
    static Ast * build(std::string jsonString);

private:
    static Ast * buildRec(const nlohmann::json & jsonObj);
    static std::vector<Ast *> buildRecVector(const nlohmann::json & jsonObj);
};

#endif // ASTBUILDER_H
