#include "ast.h"
#include "astvisitor.h"

void Integer::accept(AstVisitor * visitor)
{
    visitor->visit(this);
}

void Boolean::accept(AstVisitor * visitor)
{
    visitor->visit(this);
}

void Null::accept(AstVisitor * visitor)
{
    visitor->visit(this);
}

void Variable::accept(AstVisitor * visitor)
{
    visitor->visit(this);
}

void AccessVariable::accept(AstVisitor * visitor)
{
    visitor->visit(this);
}

void AssignVariable::accept(AstVisitor * visitor)
{
    visitor->visit(this);
}

void Function::accept(AstVisitor * visitor)
{
    visitor->visit(this);
}

void CallFunction::accept(AstVisitor * visitor)
{
    visitor->visit(this);
}

void Print::accept(AstVisitor * visitor)
{
    visitor->visit(this);
}

void Block::accept(AstVisitor * visitor)
{
    visitor->visit(this);
}

void Top::accept(AstVisitor * visitor)
{
    visitor->visit(this);
}

void Loop::accept(AstVisitor * visitor)
{
    visitor->visit(this);
}

void Conditional::accept(AstVisitor * visitor)
{
    visitor->visit(this);
}

void Object::accept(AstVisitor * visitor)
{
    visitor->visit(this);
}

void Array::accept(AstVisitor * visitor)
{
    visitor->visit(this);
}

void AssignField::accept(AstVisitor * visitor)
{
    visitor->visit(this);
}

void AssignArray::accept(AstVisitor * visitor)
{
    visitor->visit(this);
}

void AccessField::accept(AstVisitor * visitor)
{
    visitor->visit(this);
}

void AccessArray::accept(AstVisitor * visitor)
{
    visitor->visit(this);
}

void CallMethod::accept(AstVisitor * visitor)
{
    visitor->visit(this);
}
