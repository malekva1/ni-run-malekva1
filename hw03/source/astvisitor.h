#ifndef ASTVISITOR_H
#define ASTVISITOR_H

struct Ast;

struct Integer;
struct Boolean;
struct Null;
struct Variable;
struct AccessVariable;
struct AssignVariable;
struct Function;
struct CallFunction;
struct Print;
struct Block;
struct Top;
struct Loop;
struct Conditional;

struct Object;
struct Array;
struct AssignField;
struct AssignArray;
struct AccessField;
struct AccessArray;
struct CallMethod;

class AstVisitor
{
public:
    AstVisitor() = default;

    virtual void visit(Integer * node) = 0;
    virtual void visit(Boolean * node) = 0;
    virtual void visit(Null * node) = 0;
    virtual void visit(Variable * node) = 0;
    virtual void visit(AccessVariable * node) = 0;
    virtual void visit(AssignVariable * node) = 0;
    virtual void visit(Function * node) = 0;
    virtual void visit(CallFunction * node) = 0;
    virtual void visit(Print * node) = 0;
    virtual void visit(Block * node) = 0;
    virtual void visit(Top * node) = 0;
    virtual void visit(Loop * node) = 0;
    virtual void visit(Conditional * node) = 0;

    virtual void visit(Object * node) = 0;
    virtual void visit(Array * node) = 0;
    virtual void visit(AssignField * node) = 0;
    virtual void visit(AssignArray * node) = 0;
    virtual void visit(AccessField * node) = 0;
    virtual void visit(AccessArray * node) = 0;
    virtual void visit(CallMethod * node) = 0;
};

#endif // ASTVISITOR_H
