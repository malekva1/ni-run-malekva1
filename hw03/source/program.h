#ifndef PROGRAM_H
#define PROGRAM_H


#include <cstdint>
#include <string>
#include <vector>
#include <map>
#include <stack>

class Program
{
public:
    using ConstantPoolIndex = std::uint16_t;
    using GlobalsIndex = std::uint16_t;
    using LocalFrameIndex = std::uint16_t;

    Program();

    const char * data();
    int length();

    ConstantPoolIndex addIntegerToConstatnPool(int value);
    ConstantPoolIndex addBooleanToConstatnPool(bool value);
    ConstantPoolIndex addNullToConstatnPool();
    ConstantPoolIndex addStringToConstatnPool(std::string value);
    ConstantPoolIndex addSlotToConstatnPool(ConstantPoolIndex constantPoolIndex);
    ConstantPoolIndex addMethodToConstantPool(ConstantPoolIndex constantPoolIndex, std::uint8_t argumentsCount, std::uint16_t localsCount);
    ConstantPoolIndex addClassToConstantPool();

    GlobalsIndex addToGlobals(ConstantPoolIndex constantPoolIndex);
    void setEntryPoint(ConstantPoolIndex constantPoolIndex);

    void pushFunctionCode();
    void popFunctionCode();

    void addCurrentObjectMember(ConstantPoolIndex constantPoolIndex);
    void pushCurrentObjectMembers();
    void popCurrentObjectMembers();

    void setCurrentFunctionToMain(bool value);

    void addLiteralInstruction(ConstantPoolIndex index);
    void addGetLocalInstruction(LocalFrameIndex index);
    void addSetLocalInstruction(LocalFrameIndex index);
    void addGetGlobalInstruction(ConstantPoolIndex index);
    void addSetGlobalInstruction(ConstantPoolIndex index);
    void addCallFunctionInstruction(ConstantPoolIndex index, std::uint8_t argumentsCount);
    void addReturnInstruction();
    void addLabelInstruction(ConstantPoolIndex index);
    void addJumpInstruction(ConstantPoolIndex index);
    void addBranchInstruction(ConstantPoolIndex index);
    void addPrintInstruction(ConstantPoolIndex index, std::uint8_t argumentsCount);
    void addArrayInstruction();
    void addObjectInstruction(ConstantPoolIndex index);
    void addGetFieldInstruction(ConstantPoolIndex index);
    void addSetFieldInstruction(ConstantPoolIndex index);
    void addCallMethodInstruction(ConstantPoolIndex index, std::uint8_t argumentsCount);
    void addDropInstruction();

private:
    std::vector<char> constantPool;
    ConstantPoolIndex constantPoolSize = 0;

    std::vector<char> globals;
    GlobalsIndex globalsSize = 0;

    ConstantPoolIndex entryPoint;

    struct FunctionCode
    {
        std::vector<char> instructions;
        std::uint32_t instructionsCount = 0;
    };
    std::stack<FunctionCode> functionCode;
    std::vector<char> * currentFunctionCode;

    std::stack<std::vector<ConstantPoolIndex>> currentObjectMembers;

    void createCompleteProgram();
    std::vector<char> completeProgram;
    bool completeProgramCreated = false;

    std::map<int, ConstantPoolIndex> existingsInts;
    std::map<bool, ConstantPoolIndex> existingsBools;
    bool nullExists = false;
    ConstantPoolIndex existingNull;
    std::map<std::string, ConstantPoolIndex> existingsStrings;
    std::map<ConstantPoolIndex, ConstantPoolIndex> existingSlots;
    std::map<std::vector<ConstantPoolIndex>, ConstantPoolIndex> existingClasses;


    static void add16bitNumber(std::vector<char> & vector, std::uint16_t number);
};

#endif // PROGRAM_H
