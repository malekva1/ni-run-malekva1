#ifndef BCCOMPILER_H
#define BCCOMPILER_H

#include <string>

#include "astvisitor.h"
#include "program.h"
#include "localenviroment.h"

class BcCompiler : protected AstVisitor
{
public:
    static int compile(Ast * program, const std::string & outputFilePath);

    void visit(Integer * node);
    void visit(Boolean * node);
    void visit(Null * node);
    void visit(Variable * node);
    void visit(AccessVariable * node);
    void visit(AssignVariable * node);
    void visit(Function * node);
    void visit(CallFunction * node);
    void visit(Print * node);
    void visit(Block * node);
    void visit(Top * node);
    void visit(Loop * node);
    void visit(Conditional * node);

    void visit(Object * node);
    void visit(Array * node);
    void visit(AssignField * node);
    void visit(AssignArray * node);
    void visit(AccessField * node);
    void visit(AccessArray * node);
    void visit(CallMethod * node);

private:
    BcCompiler();

    Program program;
    LocalEnviroment localEnviroment;

    bool inTopBlock = true;
    bool inObject = false;
    bool inMethod = false;

    bool leaveValueOnStack = true;

    void finishMainFunction();

    unsigned int labelCount = 0;

};

#endif // BCCOMPILER_H
