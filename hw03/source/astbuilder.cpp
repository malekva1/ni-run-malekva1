#include "astbuilder.h"

#include "ast.h"

#include <iostream>

using namespace std;
using json = nlohmann::json;

Ast * AstBuilder::build(string jsonString)
{
    //cout << "INPUT: " << jsonString<< endl;

    auto jsonObj = json::parse(jsonString);
    return buildRec(jsonObj);
}

Ast * AstBuilder::buildRec(const json & jsonObj)
{
    //cout << "buildRec: " << jsonObj.dump() << endl;

    if (jsonObj.is_null())
    {
        cerr << "buildRec: null"<< endl;
    }

    if (jsonObj.is_boolean())
    {
        cerr << "buildRec: is_boolean"<< endl;
    }

    if (jsonObj.is_number())
    {
        cerr << "buildRec: is_number"<< endl;
    }

    if (jsonObj.is_object())
    {
        if (jsonObj.contains("Integer"))
        {
            Integer * res = new Integer;
            res->value = jsonObj["Integer"];
            return res;
        }

        if (jsonObj.contains("Boolean"))
        {
            Boolean * res = new Boolean;
            res->value = jsonObj["Boolean"];
            return res;
        }

        if (jsonObj.contains("Variable"))
        {
            Variable * res = new Variable;
            res->name = jsonObj["Variable"]["name"];
            res->value = buildRec(jsonObj["Variable"]["value"]);
            return res;
        }

        if (jsonObj.contains("AccessVariable"))
        {
            AccessVariable * res = new AccessVariable;
            res->name = jsonObj["AccessVariable"]["name"];
            return res;
        }

        if (jsonObj.contains("AssignVariable"))
        {
            AssignVariable * res = new AssignVariable;
            res->name = jsonObj["AssignVariable"]["name"];
            res->value = buildRec(jsonObj["AssignVariable"]["value"]);
            return res;
        }

        if (jsonObj.contains("Function"))
        {
            Function * res = new Function;
            res->name = jsonObj["Function"]["name"];
            for (auto& element : jsonObj["Function"]["parameters"])
            {
                res->parameters.push_back(element);
            }
            res->body = buildRec(jsonObj["Function"]["body"]);
            return res;
        }

        if (jsonObj.contains("CallFunction"))
        {
            CallFunction * res = new CallFunction;
            res->name = jsonObj["CallFunction"]["name"];
            res->arguments = buildRecVector(jsonObj["CallFunction"]["arguments"]);
            return res;
        }

        if (jsonObj.contains("Print"))
        {
            Print * res = new Print;
            res->format = jsonObj["Print"]["format"];
            res->arguments = buildRecVector(jsonObj["Print"]["arguments"]);
            return res;
        }

        if (jsonObj.contains("Block"))
        {
            Block * res = new Block;
            res->statements = buildRecVector(jsonObj["Block"]);
            return res;
        }

        if (jsonObj.contains("Top"))
        {
            Top * res = new Top;
            res->statements = buildRecVector(jsonObj["Top"]);
            return res;
        }

        if (jsonObj.contains("Loop"))
        {
            Loop * res = new Loop;
            res->condition = buildRec(jsonObj["Loop"]["condition"]);
            res->body = buildRec(jsonObj["Loop"]["body"]);
            return res;
        }

        if (jsonObj.contains("Conditional"))
        {
            Conditional * res = new Conditional;
            res->condition = buildRec(jsonObj["Conditional"]["condition"]);
            res->consequent = buildRec(jsonObj["Conditional"]["consequent"]);
            res->alternative = buildRec(jsonObj["Conditional"]["alternative"]);
            return res;
        }

        if (jsonObj.contains("Object"))
        {
            Object * res = new Object;
            res->extends = buildRec(jsonObj["Object"]["extends"]);
            res->members = buildRecVector(jsonObj["Object"]["members"]);
            return res;
        }

        if (jsonObj.contains("Array"))
        {
            Array * res = new Array;
            res->size = buildRec(jsonObj["Array"]["size"]);
            res->value = buildRec(jsonObj["Array"]["value"]);
            return res;
        }

        if (jsonObj.contains("AssignField"))
        {
            AssignField * res = new AssignField;
            res->object = buildRec(jsonObj["AssignField"]["object"]);
            res->field = jsonObj["AssignField"]["field"];
            res->value = buildRec(jsonObj["AssignField"]["value"]);
            return res;
        }

        if (jsonObj.contains("AssignArray"))
        {
            AssignArray * res = new AssignArray;
            res->array = buildRec(jsonObj["AssignArray"]["array"]);
            res->index = buildRec(jsonObj["AssignArray"]["index"]);
            res->value = buildRec(jsonObj["AssignArray"]["value"]);
            return res;
        }

        if (jsonObj.contains("AccessField"))
        {
            AccessField * res = new AccessField;
            res->object = buildRec(jsonObj["AccessField"]["object"]);
            res->field = jsonObj["AccessField"]["field"];
            return res;
        }

        if (jsonObj.contains("AccessArray"))
        {
            AccessArray * res = new AccessArray;
            res->array = buildRec(jsonObj["AccessArray"]["array"]);
            res->index = buildRec(jsonObj["AccessArray"]["index"]);
            return res;
        }

        if (jsonObj.contains("CallMethod"))
        {
            CallMethod * res = new CallMethod;
            res->object = buildRec(jsonObj["CallMethod"]["object"]);
            res->name = jsonObj["CallMethod"]["name"];
            res->arguments = buildRecVector(jsonObj["CallMethod"]["arguments"]);
            return res;
        }

        cerr << "buildRec: is_object"<< endl;
    }

    if (jsonObj.is_array())
    {
        cerr << "buildRec: is_array"<< endl;

    }

    if (jsonObj.is_string())
    {
        if (jsonObj.get<std::string>() == "Null")
        {
            return new Null;
        }

        cerr << "buildRec: is_string"<< endl;
    }

    cerr << "buildRec: error"<< endl;
    return NULL;
}

vector<Ast *> AstBuilder::buildRecVector(const json & jsonObj)
{
    if (jsonObj.is_array())
    {
        vector<Ast*> res;
        for (auto& element : jsonObj)
        {
            res.push_back(buildRec(element));
        }
        return res;
    }

    cerr << "buildRecVector: error"<< endl;
    vector<Ast*> res;
    return res;
}

