#include "program.h"

#include <cstring>

#include <iostream>

using namespace std;

Program::Program()
{
    functionCode.push(FunctionCode()); // main function
    currentFunctionCode = &(functionCode.top().instructions);
}

const char * Program::data()
{
    if (! completeProgramCreated)
    {
        createCompleteProgram();
    }

    return completeProgram.data();
}

int Program::length()
{
    if (! completeProgramCreated)
    {
        createCompleteProgram();
    }

    return completeProgram.size();
}

Program::ConstantPoolIndex Program::addIntegerToConstatnPool(int value)
{
    if (existingsInts.find(value) != existingsInts.end())
    {
        return existingsInts.at(value);
    }

    constantPool.push_back(0x00);
    char data[4];
    memcpy(data, &value, 4 * sizeof(char));
    constantPool.push_back(data[0]);
    constantPool.push_back(data[1]);
    constantPool.push_back(data[2]);
    constantPool.push_back(data[3]);

    ConstantPoolIndex resultIndex = constantPoolSize;
    existingsInts.insert(make_pair(value, resultIndex));
    constantPoolSize++;
    return resultIndex;
}

Program::ConstantPoolIndex Program::addBooleanToConstatnPool(bool value)
{
    if (existingsBools.find(value) != existingsBools.end())
    {
        return existingsBools.at(value);
    }

    constantPool.push_back(0x06);
    constantPool.push_back(value ? 0x01 : 0x00);

    ConstantPoolIndex resultIndex = constantPoolSize;
    existingsBools.insert(make_pair(value, resultIndex));
    constantPoolSize++;
    return resultIndex;
}

Program::ConstantPoolIndex Program::addNullToConstatnPool()
{
    if (nullExists)
    {
        return existingNull;
    }

    constantPool.push_back(0x01);
    ConstantPoolIndex resultIndex = constantPoolSize;
    nullExists = true;
    existingNull = resultIndex;
    constantPoolSize++;
    return resultIndex;
}

Program::ConstantPoolIndex Program::addStringToConstatnPool(string value)
{
    if (existingsStrings.find(value) != existingsStrings.end())
    {
        return existingsStrings.at(value);
    }

    constantPool.push_back(0x02);

    unsigned int length = value.length();

    char data[4];
    memcpy(data, &length, 4 * sizeof(char));
    constantPool.push_back(data[0]);
    constantPool.push_back(data[1]);
    constantPool.push_back(data[2]);
    constantPool.push_back(data[3]);

    for(unsigned int i = 0; i < value.length(); i++)
    {
        constantPool.push_back(value[i]);
    }

    ConstantPoolIndex resultIndex = constantPoolSize;
    existingsStrings.insert(make_pair(value, resultIndex));
    constantPoolSize++;
    return resultIndex;
}

Program::ConstantPoolIndex Program::addSlotToConstatnPool(ConstantPoolIndex constantPoolIndex)
{
    if (existingSlots.find(constantPoolIndex) != existingSlots.end())
    {
        return existingSlots.at(constantPoolIndex);
    }

    constantPool.push_back(0x04);
    add16bitNumber(constantPool, constantPoolIndex);

    ConstantPoolIndex resultIndex = constantPoolSize;
    existingSlots.insert(make_pair(constantPoolIndex, resultIndex));
    constantPoolSize++;
    return resultIndex;
}

Program::ConstantPoolIndex Program::addMethodToConstantPool(ConstantPoolIndex constantPoolIndex, std::uint8_t argumentsCount, std::uint16_t localsCount)
{
    constantPool.push_back(0x03);
    add16bitNumber(constantPool, constantPoolIndex);
    constantPool.push_back((char) argumentsCount);
    add16bitNumber(constantPool, localsCount);

    char data[4];
    memcpy(data, &(functionCode.top().instructionsCount), 4 * sizeof(char));
    constantPool.push_back(data[0]);
    constantPool.push_back(data[1]);
    constantPool.push_back(data[2]);
    constantPool.push_back(data[3]);

    constantPool.insert(constantPool.end(), currentFunctionCode->begin(), currentFunctionCode->end());

    return constantPoolSize++;
}

Program::ConstantPoolIndex Program::addClassToConstantPool()
{
    if (existingClasses.find(currentObjectMembers.top()) != existingClasses.end())
    {
        return existingClasses.at(currentObjectMembers.top());
    }

    constantPool.push_back(0x05);
    add16bitNumber(constantPool, currentObjectMembers.top().size());

    for (const auto & it : currentObjectMembers.top())
    {
        add16bitNumber(constantPool, it);
    }

    ConstantPoolIndex resultIndex = constantPoolSize;
    existingClasses.insert(make_pair(currentObjectMembers.top(), resultIndex));
    constantPoolSize++;
    return resultIndex;
}

Program::GlobalsIndex Program::addToGlobals(Program::ConstantPoolIndex constantPoolIndex)
{
    add16bitNumber(globals, constantPoolIndex);
    return globalsSize++;
}

void Program::setEntryPoint(ConstantPoolIndex constantPoolIndex)
{
    entryPoint = constantPoolIndex;
}

void Program::pushFunctionCode()
{
    functionCode.push(FunctionCode());
    currentFunctionCode = &(functionCode.top().instructions);
}

void Program::popFunctionCode()
{
    functionCode.pop();
    currentFunctionCode = &(functionCode.top().instructions);
}

void Program::addCurrentObjectMember(Program::ConstantPoolIndex constantPoolIndex)
{
    currentObjectMembers.top().push_back(constantPoolIndex);
}

void Program::pushCurrentObjectMembers()
{
    currentObjectMembers.push(vector<ConstantPoolIndex>());
}

void Program::popCurrentObjectMembers()
{
    currentObjectMembers.pop();
}

void Program::addLiteralInstruction(ConstantPoolIndex index)
{
    functionCode.top().instructionsCount++;
    currentFunctionCode->push_back(0x01);
    add16bitNumber(*currentFunctionCode, index);
}

void Program::addGetLocalInstruction(LocalFrameIndex index)
{
    functionCode.top().instructionsCount++;
    currentFunctionCode->push_back(0x0a);
    add16bitNumber(*currentFunctionCode, index);
}

void Program::addSetLocalInstruction(LocalFrameIndex index)
{
    functionCode.top().instructionsCount++;
    currentFunctionCode->push_back(0x09);
    add16bitNumber(*currentFunctionCode, index);
}

void Program::addGetGlobalInstruction(ConstantPoolIndex index)
{
    functionCode.top().instructionsCount++;
    currentFunctionCode->push_back(0xc);
    add16bitNumber(*currentFunctionCode, index);
}

void Program::addSetGlobalInstruction(ConstantPoolIndex index)
{
    functionCode.top().instructionsCount++;
    currentFunctionCode->push_back(0x0b);
    add16bitNumber(*currentFunctionCode, index);
}

void Program::addCallFunctionInstruction(ConstantPoolIndex index, uint8_t argumentsCount)
{
    functionCode.top().instructionsCount++;
    currentFunctionCode->push_back(0x08);
    add16bitNumber(*currentFunctionCode, index);
    currentFunctionCode->push_back((char) argumentsCount);
}

void Program::addReturnInstruction()
{
    functionCode.top().instructionsCount++;
    currentFunctionCode->push_back(0x0f);
}

void Program::addLabelInstruction(ConstantPoolIndex index)
{
    functionCode.top().instructionsCount++;
    currentFunctionCode->push_back(0x00);
    add16bitNumber(*currentFunctionCode, index);
}

void Program::addJumpInstruction(ConstantPoolIndex index)
{
    functionCode.top().instructionsCount++;
    currentFunctionCode->push_back(0x0e);
    add16bitNumber(*currentFunctionCode, index);
}

void Program::addBranchInstruction(ConstantPoolIndex index)
{
    functionCode.top().instructionsCount++;
    currentFunctionCode->push_back(0x0d);
    add16bitNumber(*currentFunctionCode, index);
}

void Program::addPrintInstruction(ConstantPoolIndex index, std::uint8_t argumentsCount)
{
    functionCode.top().instructionsCount++;
    currentFunctionCode->push_back(0x02);
    add16bitNumber(*currentFunctionCode, index);
    currentFunctionCode->push_back((char) argumentsCount);
}

void Program::addArrayInstruction()
{
    functionCode.top().instructionsCount++;
    currentFunctionCode->push_back(0x03);
}

void Program::addObjectInstruction(ConstantPoolIndex index)
{
    functionCode.top().instructionsCount++;
    currentFunctionCode->push_back(0x04);
    add16bitNumber(*currentFunctionCode, index);
}

void Program::addGetFieldInstruction(ConstantPoolIndex index)
{
    functionCode.top().instructionsCount++;
    currentFunctionCode->push_back(0x5);
    add16bitNumber(*currentFunctionCode, index);
}

void Program::addSetFieldInstruction(ConstantPoolIndex index)
{
    functionCode.top().instructionsCount++;
    currentFunctionCode->push_back(0x06);
    add16bitNumber(*currentFunctionCode, index);
}

void Program::addCallMethodInstruction(ConstantPoolIndex index, std::uint8_t argumentsCount)
{
    functionCode.top().instructionsCount++;
    currentFunctionCode->push_back(0x07);
    add16bitNumber(*currentFunctionCode, index);
    currentFunctionCode->push_back((char) argumentsCount);
}

void Program::addDropInstruction()
{
    functionCode.top().instructionsCount++;
    currentFunctionCode->push_back(0x10);
}

void Program::createCompleteProgram()
{
    completeProgram.clear();

    add16bitNumber(completeProgram, constantPoolSize);
    completeProgram.insert(completeProgram.end(), constantPool.begin(), constantPool.end());

    add16bitNumber(completeProgram, globalsSize);
    completeProgram.insert(completeProgram.end(), globals.begin(), globals.end());

    add16bitNumber(completeProgram, entryPoint);

    completeProgramCreated = true;
}

void Program::add16bitNumber(std::vector<char> & vector, uint16_t number)
{
    vector.push_back((char)(number & 0xff));
    vector.push_back((char)((number >> 8)& 0xff));
}

