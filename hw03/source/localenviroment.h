#ifndef LOCALENVIROMENT_H
#define LOCALENVIROMENT_H

#include <vector>
#include <map>
#include <string>
#include <cstdint>

#include "program.h"

class LocalEnviroment
{
public:
    LocalEnviroment() = default;

    Program::LocalFrameIndex addVariable(const std::string & name);

    Program::LocalFrameIndex findVariable(const std::string & name) const;

    void addLevel();
    void removeLevel();

    class VariableDoesntExistException{};

    Program::LocalFrameIndex size() const;

private:
    Program::LocalFrameIndex lastIndex = 0;

    using Level = std::map<std::string, Program::LocalFrameIndex>;
    std::vector<Level> variableStack;
};

#endif // LOCALENVIROMENT_H
