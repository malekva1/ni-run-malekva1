#include "bccompiler.h"

#include <iostream>
#include <fstream>

#include "ast.h"

using namespace std;

BcCompiler::BcCompiler()
{

}

int BcCompiler::compile(Ast *program, const string & outputFilePath)
{
    BcCompiler compiler;

    try
    {
        compiler.program.addStringToConstatnPool("λ:");
        program->accept(&compiler);
        compiler.finishMainFunction();
    }
    catch (int errorNum)
    {
        return errorNum;
    }

    //write to file
    ofstream outputFile;
    outputFile.open(outputFilePath, ios::out | ios::binary);
    if (! outputFile.is_open())
    {
        cerr << "Unable to open output file for writing." << endl;
        return 1;
    }
    outputFile.write(compiler.program.data(), compiler.program.length());
    if (! outputFile.good())
    {
        cerr << "Unable to write to output file." << endl;
        return 2;
    }
    outputFile.close();

    return 0;
}

void BcCompiler::visit(Integer * node)
{
    auto constantPoolIndex= program.addIntegerToConstatnPool(node->value);
    program.addLiteralInstruction(constantPoolIndex);

    if (! leaveValueOnStack)
    {
        program.addDropInstruction();
    }
}

void BcCompiler::visit(Boolean * node)
{
    auto constantPoolIndex = program.addBooleanToConstatnPool(node->value);
    program.addLiteralInstruction(constantPoolIndex);

    if (! leaveValueOnStack)
    {
        program.addDropInstruction();
    }
}

void BcCompiler::visit(Null *)
{
    auto constantPoolIndex = program.addNullToConstatnPool();
    program.addLiteralInstruction(constantPoolIndex);

    if (! leaveValueOnStack)
    {
        program.addDropInstruction();
    }
}

void BcCompiler::visit(Variable * node)
{
    bool leaveValueOnStackBackup = leaveValueOnStack;

    leaveValueOnStack = true;
    node->value->accept(this);

    if (inTopBlock || (inObject && ! inMethod))
    {
        auto nameIndex = program.addStringToConstatnPool(node->name);
        auto slotIndex = program.addSlotToConstatnPool(nameIndex);

        if (inObject)
        {
            program.addCurrentObjectMember(slotIndex);
        }
        else
        {
            program.addSetGlobalInstruction(nameIndex);
            program.addToGlobals(slotIndex);
        }
    }
    else
    {
        auto variableIndex = localEnviroment.addVariable(node->name);
        program.addSetLocalInstruction(variableIndex);
    }

    leaveValueOnStack = leaveValueOnStackBackup;

    if (! leaveValueOnStackBackup)
    {
        program.addDropInstruction();
    }
}

void BcCompiler::visit(AccessVariable * node)
{
    try
    {
        auto localFrameIndex = localEnviroment.findVariable(node->name);
        program.addGetLocalInstruction(localFrameIndex);
    }
    catch (LocalEnviroment::VariableDoesntExistException)
    {
        auto nameIndex = program.addStringToConstatnPool(node->name);
        program.addGetGlobalInstruction(nameIndex);
    }

    if (! leaveValueOnStack)
    {
        program.addDropInstruction();
    }
}

void BcCompiler::visit(AssignVariable * node)
{
    bool leaveValueOnStackBackup = leaveValueOnStack;

    leaveValueOnStack = true;
    node->value->accept(this);

    try
    {
        auto localFrameIndex = localEnviroment.findVariable(node->name);
        program.addSetLocalInstruction(localFrameIndex);
    }
    catch (LocalEnviroment::VariableDoesntExistException)
    {
        auto nameIndex = program.addStringToConstatnPool(node->name);
        program.addSetGlobalInstruction(nameIndex);
    }

    if (! leaveValueOnStackBackup)
    {
        program.addDropInstruction();
    }
}

void BcCompiler::visit(Function * node)
{
    program.pushFunctionCode();
    LocalEnviroment localEnviromentBackup = localEnviroment;
    localEnviroment = LocalEnviroment();
    localEnviroment.addLevel();

    if (inObject)
    {
        inMethod = true;
        localEnviroment.addVariable("this");
    }

    for(const auto & it :node->parameters)
    {
        localEnviroment.addVariable(it);
    }

    leaveValueOnStack = true;
    node->body->accept(this);

    program.addReturnInstruction();

    auto nameIndex = program.addStringToConstatnPool(node->name);

    int numberOfArguments = node->parameters.size();
    Program::LocalFrameIndex numberOfLocals = localEnviroment.size() - numberOfArguments;

    if (inObject)
    {
        auto methodIndex = program.addMethodToConstantPool(nameIndex, numberOfArguments + 1, numberOfLocals - 1);
        program.addCurrentObjectMember(methodIndex);
    }
    else
    {
        auto methodIndex = program.addMethodToConstantPool(nameIndex, numberOfArguments, numberOfLocals);
        program.addToGlobals(methodIndex);
    }

    program.popFunctionCode();
    localEnviroment = localEnviromentBackup;
    inMethod = false;
}

void BcCompiler::visit(CallFunction * node)
{
    bool leaveValueOnStackBackup = leaveValueOnStack;

    auto nameIndex = program.addStringToConstatnPool(node->name);

    for (auto & it : node->arguments)
    {
        leaveValueOnStack = true;
        it->accept(this);
    }

    int numberOfArguments = node->arguments.size();

    program.addCallFunctionInstruction(nameIndex, numberOfArguments);

    if (! leaveValueOnStackBackup)
    {
        program.addDropInstruction();
    }
}

void BcCompiler::visit(Print * node)
{
    bool leaveValueOnStackBackup = leaveValueOnStack;

    auto formatStringIndex = program.addStringToConstatnPool(node->format);

    for (auto & it : node->arguments)
    {
        leaveValueOnStack = true;
        it->accept(this);
    }

    int numberOfArguments = node->arguments.size();

    program.addPrintInstruction(formatStringIndex, numberOfArguments);

    if (! leaveValueOnStackBackup)
    {
        program.addDropInstruction();
    }
}

void BcCompiler::visit(Block * node)
{
    bool leaveValueOnStackBackup = leaveValueOnStack;

    bool inTopBlockBackup = inTopBlock;
    inTopBlock = false;
    localEnviroment.addLevel();

    for (int i = 0; i < node->statements.size(); i++)
    {
        if (i == node->statements.size() - 1)
        {
            leaveValueOnStack = leaveValueOnStackBackup;
        }
        else
        {
            leaveValueOnStack = false;
        }
        node->statements[i]->accept(this);
    }

    inTopBlock = inTopBlockBackup;
    localEnviroment.removeLevel();
}

void BcCompiler::visit(Top * node)
{
    inTopBlock = true;
    localEnviroment.addLevel();

    for (int i = 0; i < node->statements.size(); i++)
    {
        if (i == node->statements.size()-1)
        {
            leaveValueOnStack = true;
        }
        else
        {
            leaveValueOnStack = false;
        }
        node->statements[i]->accept(this);
    }
}

void BcCompiler::visit(Loop * node)
{
    bool leaveValueOnStackBackup = leaveValueOnStack;

    auto bodyLabelIndex = program.addStringToConstatnPool("loop:body:" + to_string(labelCount));
    auto conditionLabelIndex = program.addStringToConstatnPool("loop:condition:" + to_string(labelCount));
    labelCount++;


    program.addJumpInstruction(conditionLabelIndex);
    program.addLabelInstruction(bodyLabelIndex);

    leaveValueOnStack = false;
    node->body->accept(this);

    program.addLabelInstruction(conditionLabelIndex);

    leaveValueOnStack = true;
    node->condition->accept(this);

    program.addBranchInstruction(bodyLabelIndex);

    if (leaveValueOnStackBackup)
    {
        auto nullIndex = program.addNullToConstatnPool();
        program.addLiteralInstruction(nullIndex);
    }
}

void BcCompiler::visit(Conditional * node)
{
    bool leaveValueOnStackBackup = leaveValueOnStack;

    auto thenLabelIndex = program.addStringToConstatnPool("if:consequent:" + to_string(labelCount));
    auto afterLabelIndex = program.addStringToConstatnPool("if:end:" + to_string(labelCount));
    labelCount++;

    leaveValueOnStack = true;
    node->condition->accept(this);

    program.addBranchInstruction(thenLabelIndex);

    leaveValueOnStack = leaveValueOnStackBackup;
    node->alternative->accept(this);
    program.addJumpInstruction(afterLabelIndex);

    program.addLabelInstruction(thenLabelIndex);
    leaveValueOnStack = leaveValueOnStackBackup;
    node->consequent->accept(this);

    program.addLabelInstruction(afterLabelIndex);
}

void BcCompiler::visit(Object * node)
{
    bool leaveValueOnStackBackup = leaveValueOnStack;

    leaveValueOnStack = true;
    node->extends->accept(this);

    bool inObjectBackup = inObject;
    inObject = true;

    bool inMethodBackup = inMethod;
    inMethod = false;

    program.pushCurrentObjectMembers();
    for (auto i : node->members)
    {
        leaveValueOnStack = true;
        i->accept(this);
    }
    auto classIndex = program.addClassToConstantPool();

    program.popCurrentObjectMembers();
    inObject = inObjectBackup;
    inMethod = inMethodBackup;

    program.addObjectInstruction(classIndex);

    if (! leaveValueOnStackBackup)
    {
        program.addDropInstruction();
    }
}

void BcCompiler::visit(Array * node)
{
    bool leaveValueOnStackBackup = leaveValueOnStack;

    if(dynamic_cast<Integer*>(node->value)||
       dynamic_cast<Boolean*>(node->value)||
       dynamic_cast<Null*>(node->value)||
       dynamic_cast<AccessVariable*>(node->value)||
       dynamic_cast<AssignField*>(node->value))
    {
        leaveValueOnStack = true;
        node->size->accept(this);
        leaveValueOnStack = true;
        node->value->accept(this);
        program.addArrayInstruction();

        if (! leaveValueOnStackBackup)
        {
            program.addDropInstruction();
        }

        return;
    }

    Variable sizeVar;
    sizeVar.name = "::size";
    sizeVar.value = node->size;
    leaveValueOnStack = false;
    sizeVar.accept(this);

    AccessVariable getSizeVar;
    getSizeVar.name = "::size";

    Null nullVar;

    Array arrayOfNulls;
    arrayOfNulls.size = &getSizeVar;
    arrayOfNulls.value = &nullVar;

    Variable arrayVar;
    sizeVar.name = "::array";
    sizeVar.value = &arrayOfNulls;
    leaveValueOnStack = false;
    sizeVar.accept(this);

    Integer zero;
    zero.value = 0;

    Variable iVar;
    iVar.name = "::i";
    iVar.value = &zero;
    leaveValueOnStack = false;
    iVar.accept(this);

    auto bodyLabelIndex = program.addStringToConstatnPool("loop:body:" + to_string(labelCount));
    auto conditionLabelIndex = program.addStringToConstatnPool("loop:condition:" + to_string(labelCount));
    labelCount++;

    program.addJumpInstruction(conditionLabelIndex);
    program.addLabelInstruction(bodyLabelIndex);

    AccessVariable getArrayVar;
    getArrayVar.name = "::array";

    AccessVariable getIVar;
    getIVar.name = "::i";

    AssignArray assignArray;
    assignArray.array = &getArrayVar;
    assignArray.index = &getIVar;
    assignArray.value = node->value;
    leaveValueOnStack = false;
    assignArray.accept(this);

    Integer one;
    one.value = 1;

    CallMethod incrementedI;
    incrementedI.object = &getIVar;
    incrementedI.name = "+";
    incrementedI.arguments.push_back(&one);

    AssignVariable incrementI;
    incrementI.name = "::i";
    incrementI.value = &incrementedI;
    leaveValueOnStack = false;
    incrementI.accept(this);

    program.addLabelInstruction(conditionLabelIndex);

    CallMethod iSmallerThenSize;
    iSmallerThenSize.object = &getIVar;
    iSmallerThenSize.name = "<";
    iSmallerThenSize.arguments.push_back(&getSizeVar);
    leaveValueOnStack = true;
    iSmallerThenSize.accept(this);

    program.addBranchInstruction(bodyLabelIndex);
    leaveValueOnStack = true;
    getArrayVar.accept(this);

    // this probably should be here, but I want same result as Konrad's compiler

    /*
    if (! leaveValueOnStackBackup)
    {
        program.addDropInstruction();
    }
    */
}

void BcCompiler::visit(AssignField * node)
{
    bool leaveValueOnStackBackup = leaveValueOnStack;

    leaveValueOnStack = true;
    node->object->accept(this);
    leaveValueOnStack = true;
    node->value->accept(this);

    auto nameIndex = program.addStringToConstatnPool(node->field);

    program.addSetFieldInstruction(nameIndex);

    if (! leaveValueOnStackBackup)
    {
        program.addDropInstruction();
    }
}

void BcCompiler::visit(AssignArray * node)
{
    bool leaveValueOnStackBackup = leaveValueOnStack;

    leaveValueOnStack = true;
    node->array->accept(this);

    leaveValueOnStack = true;
    node->index->accept(this);

    leaveValueOnStack = true;
    node->value->accept(this);

    auto setIndex = program.addStringToConstatnPool("set");
    program.addCallMethodInstruction(setIndex, 3);

    if (! leaveValueOnStackBackup)
    {
        program.addDropInstruction();
    }
}

void BcCompiler::visit(AccessField * node)
{
    bool leaveValueOnStackBackup = leaveValueOnStack;

    leaveValueOnStack = true;
    node->object->accept(this);

    auto nameIndex = program.addStringToConstatnPool(node->field);

    program.addGetFieldInstruction(nameIndex);

    if (! leaveValueOnStackBackup)
    {
        program.addDropInstruction();
    }
}

void BcCompiler::visit(AccessArray * node)
{
    bool leaveValueOnStackBackup = leaveValueOnStack;

    leaveValueOnStack = true;
    node->array->accept(this);

    leaveValueOnStack = true;
    node->index->accept(this);

    auto getIndex = program.addStringToConstatnPool("get");
    program.addCallMethodInstruction(getIndex, 2);

    if (! leaveValueOnStackBackup)
    {
        program.addDropInstruction();
    }
}

void BcCompiler::visit(CallMethod * node)
{
    bool leaveValueOnStackBackup = leaveValueOnStack;

    auto nameIndex = program.addStringToConstatnPool(node->name);

    leaveValueOnStack = true;
    node->object->accept(this);

    for (auto & it : node->arguments)
    {
        leaveValueOnStack = true;
        it->accept(this);
    }

    int numberOfArguments = node->arguments.size() + 1;

    program.addCallMethodInstruction(nameIndex, numberOfArguments);

    if (! leaveValueOnStackBackup)
    {
        program.addDropInstruction();
    }
}

void BcCompiler::finishMainFunction()
{
    auto mainFunctionIdex = program.addMethodToConstantPool(0, 0, localEnviroment.size());
    program.setEntryPoint(mainFunctionIdex);
}
