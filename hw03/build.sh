#!/bin/bash

mkdir -p build

cd build
cmake -S ../source/ -B .
make $@

rsync -t bccompiler ../bccompiler
