if [ "$#" -lt 2 ];
then
    echo "Usage:"
    echo " - "$0" run source.fml"
    exit 1
fi

ABSOLUTE_PATH="$(realpath "$0")"
SCRIPT_PATH="$(dirname "$ABSOLUTE_PATH")"

if [ "$1" == "run" ]
then

    source="$2"
    json=$(mktemp)

    "$SCRIPT_PATH"/reference.fml parse "$source" --format=json -o "$json"
    "$SCRIPT_PATH"/hw01/interpreter.run "$json"

    exit 0
fi

>&2 echo "Unsuported command $1"
exit 1

