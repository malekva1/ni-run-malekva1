#!/bin/bash

cd "$(dirname "$0")"

mkdir -p build

cd build
cmake -S ../source/ -B .
make $@

rsync -t runtime ../runtime
