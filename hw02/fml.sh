#!/bin/bash

ABSOLUTE_PATH="$(realpath "$0")"
SCRIPT_PATH="$(dirname "$ABSOLUTE_PATH")"

if [ "$1" != "execute" ]
then 
    >&2 echo "Unsuported command $1"
    exit 1
fi

INPUTFILE="$2" 

"$SCRIPT_PATH"/runtime "$INPUTFILE"
