#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H

#include <vector>
#include <string>
#include <variant>

#include "programobject.h"
#include "pointer.h"
#include "constantpoolindex.h"

struct Null{};

struct Array
{
    std::vector<Pointer> data;
};

struct Object
{
    Pointer parent;
    using FieldsMap = std::vector<std::pair<std::string, Pointer>>;
    FieldsMap fields;

    using MethodsMap = std::vector<std::pair<std::string, ConstantPoolIndex>>;
    MethodsMap methods;

    Object(Pointer parent, Object::FieldsMap fields, Object::MethodsMap methods);
};

class Heap;

class RuntimeObject
{
public:
    RuntimeObject(const ProgramObject & programObject);
    RuntimeObject(); // creates null
    RuntimeObject(bool value); // creates boolean
    RuntimeObject(int value); // creates integer
    RuntimeObject(int size, Pointer initializerPointer); // creates array
    RuntimeObject(Pointer parent, Object::FieldsMap fields, Object::MethodsMap methods); // creates object

    bool isInteger() const;
    bool isBoolean() const;
    bool isNull() const;
    bool isArray() const;
    bool isObject() const;

    int toInteger() const;
    bool toBoolean() const;

    const Array & toArray() const;
    const Object & toObject() const;

    std::string toString(const Heap * heap) const;

    Pointer getArrayField(int index) const;
    void setArrayField(int index, Pointer value);

    Pointer getObjectField(const std::string & name) const;
    void setObjectField(const std::string & name, Pointer value);

    Pointer objectFindMethodReceiver(const std::string & methodName, Pointer currentPointer, const Heap * heap) const;
    ConstantPoolIndex objectFindMethod(const std::string & name) const;

    unsigned int size() const;
private:
    std::variant<int, bool, Null, Array, Object> object;

};

/*
#include <vector>
#include <string>
#include <variant>

#include "programobject.h"
#include "pointer.h"
#include "constantpoolindex.h"


class RuntimeObject
{
public:
    RuntimeObject(const ProgramObject & programObject);
    RuntimeObject(); // creates null
    RuntimeObject(bool value); // creates boolean
    RuntimeObject(int value); // creates integer
    RuntimeObject(int size, Pointer initializerPointer); // creates array
    RuntimeObject(Pointer parent, Object::FieldsMap fields, Object::MethodsMap methods); // creates object

    virtual bool isInteger() const;
    virtual bool isBoolean() const;
    virtual bool isNull() const;
    virtual bool isArray() const;
    virtual bool isObject() const;

    int toInteger() const;
    bool toBoolean() const;

    const Array & toArray() const;
    const Object & toObject() const;

    std::string toString(const Heap * heap) const;

    Pointer getArrayField(int index) const;
    void setArrayField(int index, Pointer value);

    Pointer getObjectField(const std::string & name) const;
    void setObjectField(const std::string & name, Pointer value);

    Pointer objectFindMethodReceiver(const std::string & methodName, Pointer currentPointer, const Heap * heap) const;
    ConstantPoolIndex objectFindMethod(const std::string & name) const;

    virtual unsigned int size() const = 0;
private:

};


class Integer : public RuntimeObject
{
public:
    bool isInteger() const;
private:
    int value;
};

class Boolean : public RuntimeObject
{
public:
    bool isBoolean() const;
private:
    bool value;
};

class Null : public RuntimeObject
{};

class Array : public RuntimeObject
{
    std::vector<Pointer> data;
};

struct Object : public RuntimeObject
{
    Pointer parent;
    using FieldsMap = std::vector<std::pair<std::string, Pointer>>;
    FieldsMap fields;

    using MethodsMap = std::vector<std::pair<std::string, ConstantPoolIndex>>;
    MethodsMap methods;

    Object(Pointer parent, Object::FieldsMap fields, Object::MethodsMap methods);
};

*/

#endif // RUNTIMEOBJECT_H
