#include "constantpool.h"

#include <iostream>

using namespace std;

void ConstantPool::add(ProgramObject object)
{
    data.push_back(object);
}

const ProgramObject & ConstantPool::operator[] (const ConstantPoolIndex & index) const
{
    return data[index.value];
}

void ConstantPool::print() const
{
    for(unsigned int i = 0; i < data.size(); i++)
    {
        cout << "#" << i << "\t| " << data[i].toDebugString() << endl;
    }
}
