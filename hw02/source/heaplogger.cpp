#include "heaplogger.h"

#include <iostream>
#include <fstream>

using namespace std;

void HeapLoger::addStart()
{
    if (doHeapLog)
    {
        startTime = timeNow();
        log.push_back(LogItem{0, startTime, EventType::start});
    }
}

void HeapLoger::addAllocation(unsigned int size)
{
    if (doHeapLog)
    {
        log.push_back(LogItem{size, timeNow(), EventType::allcation});
    }
}

void HeapLoger::addGC(unsigned int size)
{
    if (doHeapLog)
    {
        log.push_back(LogItem{size, timeNow(), EventType::gc});
    }
}

void HeapLoger::writeLog() const
{
    if (doHeapLog)
    {
        ofstream logFile;
        logFile.open(heapLogPath, ios_base::app);
        if (! logFile.is_open())
        {
            cerr << "Unable to write to file " << heapLogPath << "." << endl;
        }

        for (const auto & it : log)
        {
            chrono::nanoseconds duration = it.timestamp - startTime;
            string event = it.eventType == EventType::start ? "S" : it.eventType == EventType::allcation ? "A" : "G";
            logFile << duration.count() << "," << event << "," << it.size <<endl;
        }

        logFile.close();
    }
}

chrono::time_point<std::chrono::steady_clock> HeapLoger::timeNow() const
{
    return chrono::steady_clock::now();
}
