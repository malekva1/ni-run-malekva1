#ifndef FUNCTIONTABLE_H
#define FUNCTIONTABLE_H

#include <map>
#include <string>

#include "constantpoolindex.h"

class FunctionTable : public std::map<std::string, ConstantPoolIndex>
{
public:
    FunctionTable() = default;
    void add(std::string name, ConstantPoolIndex index);
};

#endif // FUNCTIONTABLE_H
