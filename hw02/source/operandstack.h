#ifndef OPERANDSTACK_H
#define OPERANDSTACK_H

#include <vector>

#include "pointer.h"

class Heap;

class OperandStack
{
public:
    void print(Heap * heap) const;

    void push(Pointer pointer);
    Pointer top() const;
    void pop();
    bool empty() const;

private:
    std::vector<Pointer> data;
    friend class GarbageCollector;
};

#endif // OPERANDSTACK_H
