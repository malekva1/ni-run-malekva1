#include "virtualmachine.h"

#include <string>

#include "instructions.h"

#include <iostream>

using namespace std;

void VirtualMachine::run(const Program & program)
{
    VirtualMachine vm(program);

    vm.findLabels();
    vm.processGlobals();

    while(vm.canDoStep())
    {
        //unsigned int dummy = 0;
        //cout << endl << "INSTRUCTION: (" << vm.instructionPointer.value << ") " << printInstruction(vm.program.getCode(), vm.instructionPointer.value, dummy) << endl;

        vm.doStep();

        //vm.operandStack.print(&vm.heap);
        //vm.frameStack.print();

        //vm.heap.gc.collectGarbage();
    }
}

VirtualMachine::VirtualMachine(const Program & _program)
    : program(_program)
    , heap(this)
    , instructionPointer(&(program.getCode()))
{
    ProgramObject firstMethod = constantPool(program.getEntryPoint());

    if(! firstMethod.isMethod())
    {
        throw Exception("Entry point does not point to method.");
    }

    frameValues.clear();
    for (unsigned int i = 0; i < firstMethod.methodLocals(); i++)
    {
        frameValues.push_back(heap.newNull());
    }

    frameStack.push(instructionPointer, frameValues);
    frameValues.clear();

    instructionPointer.set(firstMethod.methodStart());
}

void VirtualMachine::findLabels()
{
    InstructionPointer currentInstruction(&(program.getCode()));

    while (currentInstruction.isValid())
    {
        Byte opCode = code(currentInstruction);

        if(opCode == LABEL_OPCODE)
        {
            ConstantPoolIndex nameIndex(code(currentInstruction + 1),
                                        code(currentInstruction + 2));

            ProgramObject object = constantPool(nameIndex);
            if (! object.isString())
            {
                throw Exception("Label value is not string, instruction pointer: " + currentInstruction.toString() + ".");
            }
            string name = object.toString();

            labelTable.add(name, currentInstruction);
        }
        currentInstruction.bump();
    }
}

void VirtualMachine::processGlobals()
{
    unsigned int globalsIndex = 0;
    for (unsigned int i = 0; i <  program.getGlobals().size(); i++)
    {
        ConstantPoolIndex objectIndex = program.getGlobals()[i];
        ProgramObject object = constantPool(objectIndex);

        if (object.isSlot())
        {
            ConstantPoolIndex nameIndex = object.slotValue();
            ProgramObject nameObject = constantPool(nameIndex);

            if (! nameObject.isString())
            {
                throw Exception("Name of global variable is not a string.");
            }
            string name = nameObject.toString();

            globalsTable.add(name, globalsIndex);
            globalsIndex++;
        }
        else if (object.isMethod())
        {
            ConstantPoolIndex nameIndex = object.methodNameIndex();
            ProgramObject nameObject = constantPool(nameIndex);
            if (! nameObject.isString())
            {
                throw Exception("Name of function is not a string.");
            }
            string name = nameObject.toString();
            functionTable.add(name, objectIndex);
        }
        else
        {
            throw Exception("Pointer in globals in position " + to_string(i) + "does not point to slot nor method.");
        }
    }

    frameValues.clear();
    for (unsigned int i = 0; i < globalsIndex; i++)
    {
        frameValues.push_back(heap.newNull());
    }
    frameStack.setGlobals(frameValues);
    frameValues.clear();
}


bool VirtualMachine::canDoStep()
{
    return instructionPointer.isValid();
}

void VirtualMachine::doStep()
{
    Byte opCode = code(instructionPointer);

    switch (opCode.toChar())
    {
        case LABEL_OPCODE:
        {
            evalLabel(); break;
        }
        case LITERAL_OPCODE:
        {
            evalLiteral(); break;
        }
        case PRINT_OPCODE:
        {
            evalPrint(); break;
        }
        case ARRAY_OPCODE:
        {
            evalArray(); break;
        }
        case OBJECT_OPCODE:
        {
            evalObject(); break;
        }
        case GETSLOT_OPCODE:
        {
            evalGetSlot(); break;
        }
        case SETSLOT_OPCODE:
        {
            evalSetSlot(); break;
        }
        case CALLMETHOD_OPCODE:
        {
            evalCallMethod(); break;
        }
        case CALLFUNCTION_OPCODE:
        {
            evalCallFunction(); break;
        }
        case SETLOCAL_OPCODE:
        {
            evalSetLocal(); break;
        }
        case GETLOCAL_OPCODE:
        {
            evalGetLocal(); break;
        }
        case SETGLOBAL_OPCODE:
        {
            evalSetGlobal(); break;
        }
        case GETGLOBAL_OPCODE:
        {
            evalGetGlobal(); break;
        }
        case BRANCH_OPCODE:
        {
            evalBranch(); break;
        }
        case JUMP_OPCODE:
        {
            evalJump(); break;
        }
        case RETURN_OPCODE:
        {
            evalReturn(); break;
        }
        case DROP_OPCODE:
        {
            evalDrop(); break;
        }
        default:
        {
            throw Exception("Unsupported instruction, opcode " + to_string(int8value(opCode)) + ".");
        }
    }
}

const ProgramObject & VirtualMachine::constantPool(const ConstantPoolIndex & index) const
{
    return program.getConstantPool()[index];
}


const Byte & VirtualMachine::code(const InstructionPointer & pointer) const
{
    return program.getCode()[pointer];
}

VirtualMachine::Exception::Exception(string message) : message(message)
{
}
