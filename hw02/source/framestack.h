#ifndef FRAMESTACK_H
#define FRAMESTACK_H

#include <vector>

#include "pointer.h"
#include "instructionpointer.h"

class FrameStack
{
    struct GlobalFrame
    {
        std::vector<Pointer> locals;
    };

    struct LocalFrame
    {
        LocalFrame(InstructionPointer returnAddress, std::vector<Pointer> locals);
        InstructionPointer returnAddress;
        std::vector<Pointer> locals;
    };

public:
    FrameStack() = default;

    void push(InstructionPointer returnAddress, const std::vector<Pointer> & initialValues);
    void pop();

    Pointer local(unsigned int index) const;
    void setLocal(unsigned int index, Pointer value);

    void setGlobals(const std::vector<Pointer> & initialValues);
    Pointer global(unsigned int index) const;
    void setGlobal(unsigned int index, Pointer value);

    InstructionPointer getReturnAddress();

    void print() const;
private:
    friend class GarbageCollector;

    GlobalFrame globalFrame;
    std::vector<LocalFrame> localFrames;
};

#endif // FRAMESTACK_H
