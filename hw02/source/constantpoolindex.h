#ifndef CONSTANTPOOLINDEX_H
#define CONSTANTPOOLINDEX_H

#include "byte.h"

class ConstantPoolIndex
{
public:
    ConstantPoolIndex() = default;
    ConstantPoolIndex(Byte first, Byte second);

    std::string toString() const;

private:
    friend class ConstantPool;
    unsigned int value;
};

#endif // CONSTANTPOOLINDEX_H
