#ifndef VIRTUALMACHINE_H
#define VIRTUALMACHINE_H

#include "program.h"
#include "constantpool.h"
#include "globals.h"
#include "constantpoolindex.h"
#include "code.h"
#include "byte.h"

#include "heap.h"
#include "operandstack.h"
#include "framestack.h"
#include "labeltable.h"
#include "globalstable.h"
#include "functiontable.h"
#include "instructionpointer.h"

class VirtualMachine
{
public:
    static void run(const Program & program);

    struct Exception
    {
        Exception (std::string message);
        std::string message;
    };

private:
    VirtualMachine(const Program & program);

    void findLabels();
    void processGlobals();

    bool canDoStep();
    void doStep();

    const Program program;

    const ProgramObject & constantPool(const ConstantPoolIndex & index) const;
    const ConstantPool & constantPool() const;
    const Globals & globals() const;
    const Byte & code(const InstructionPointer & pointer) const;

    Heap heap;
    OperandStack operandStack;
    FrameStack frameStack;
    InstructionPointer instructionPointer;

    LabelTable labelTable;
    GlobalsTable globalsTable;
    FunctionTable functionTable;

    void evalLabel();
    void evalLiteral();
    void evalPrint();
    void evalArray();
    void evalObject();
    void evalGetSlot();
    void evalSetSlot();
    void evalCallMethod();
    void evalCallFunction();
    void evalSetLocal();
    void evalGetLocal();
    void evalSetGlobal();
    void evalGetGlobal();
    void evalBranch();
    void evalJump();
    void evalReturn();
    void evalDrop();

    friend class GarbageCollector;
    // currentValues
    std::vector<Pointer> frameValues;
    Pointer * currentPointer = nullptr;


};

#endif // VIRTUALMACHINE_H
