#include "pointer.h"

using namespace std;

Pointer::Pointer(size_t value) : value(value)
{
}

string Pointer::toString() const
{
    return "0x" + to_string(value);
}
