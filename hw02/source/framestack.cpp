#include "framestack.h"

#include "virtualmachine.h"

#include <iostream>

using namespace std;

FrameStack::LocalFrame::LocalFrame(InstructionPointer returnAddress, std::vector<Pointer> locals)
    : returnAddress(returnAddress)
    , locals(locals)
{
}

void FrameStack::push(InstructionPointer returnAddress, const std::vector<Pointer> & initialValues)
{
    LocalFrame newFrame(returnAddress, initialValues);
    localFrames.push_back(newFrame);
}

void FrameStack::pop()
{
    localFrames.pop_back();
}

Pointer FrameStack::local(unsigned int index) const
{
    if (index >= localFrames.back().locals.size())
    {
        throw VirtualMachine::Exception("Trying to read invalid local variable.");
    }

    return localFrames.back().locals[index];
}

void FrameStack::setLocal(unsigned int index, Pointer value)
{
    if (index >= localFrames.back().locals.size())
    {
        throw VirtualMachine::Exception("Trying to set invalid local variable.");
    }

    localFrames.back().locals[index] = value;
}

void FrameStack::setGlobals(const std::vector<Pointer> & initialValues)
{
    globalFrame.locals = initialValues;
}

Pointer FrameStack::global(unsigned int index) const
{
    if (index >= globalFrame.locals.size())
    {
        throw VirtualMachine::Exception("Trying to read invalid global variable.");
    }

    return globalFrame.locals[index];
}

void FrameStack::setGlobal(unsigned int index, Pointer value)
{
    if (index >= globalFrame.locals.size())
    {
        throw VirtualMachine::Exception("Trying to set invalid global variable.");
    }

    globalFrame.locals[index] = value;
}

InstructionPointer FrameStack::getReturnAddress()
{
    return localFrames.back().returnAddress;
}

void FrameStack::print() const
{
    cout << "GLOBALS:" << endl;
    for (const auto & it : globalFrame.locals)
    {
        cout << it.toString() + ", ";
    }
    cout << endl;

    cout << "LOCALS:" << endl;
    for (const auto & frame : localFrames)
    {
        for (const auto & it : frame.locals)
        {
            cout << it.toString() + ", ";
        }
        cout << endl;
    }
    cout << endl;
}
