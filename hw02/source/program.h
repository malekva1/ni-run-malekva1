#ifndef PROGRAM_H
#define PROGRAM_H

#include <vector>

#include "byte.h"
#include "constantpool.h"
#include "globals.h"
#include "constantpoolindex.h"
#include "code.h"

class Program
{
public:
    Program(std::vector<Byte> bytecode);

    const ConstantPool & getConstantPool() const;
    const Globals & getGlobals() const;
    const ConstantPoolIndex & getEntryPoint() const;
    const Code & getCode() const;

    void print() const;

private:
    ConstantPool constantPool;
    Globals globals;
    ConstantPoolIndex entryPoint;
    Code code;
};

#endif // PROGRAM_H
