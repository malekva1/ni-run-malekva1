#include "runtimeobject.h"


#include "virtualmachine.h"
#include "heap.h"

using namespace std;

Object::Object(Pointer parent, FieldsMap fields, MethodsMap methods)
    : parent(parent)
    , fields(fields)
    , methods(methods)
{
}

RuntimeObject::RuntimeObject(const ProgramObject & programObject)
{
    if (programObject.isInteger())
    {
        object = programObject.toInteger();
    }
    else if (programObject.isBoolean())
    {
        object = programObject.toBoolean();
    }
    else if (programObject.isNull())
    {
        object = Null();
    }
    else
    {
        throw VirtualMachine::Exception("Creating RuntimeObject from unsupported ProgramObject");
    }
}

RuntimeObject::RuntimeObject()
{
    object = Null();
}

RuntimeObject::RuntimeObject(bool value)
{
    object = value;
}

RuntimeObject::RuntimeObject(int value)
{
    object = value;
}

RuntimeObject::RuntimeObject(int size, Pointer initializerPointer)
{
    Array array;
    for(int i = 0; i < size; i++)
    {
        array.data.push_back(initializerPointer);
    }
    object = array;
}

RuntimeObject::RuntimeObject(Pointer parent, Object::FieldsMap fields, Object::MethodsMap methods)
{
    Object newObject(parent, fields, methods);
    object = newObject;
}

bool RuntimeObject::isInteger() const
{
    return object.index() == 0;
}

bool RuntimeObject::isBoolean() const
{
    return object.index() == 1;
}

bool RuntimeObject::isNull() const
{
    return object.index() == 2;
}

bool RuntimeObject::isArray() const
{
    return object.index() == 3;
}

bool RuntimeObject::isObject() const
{
    return object.index() == 4;
}

int RuntimeObject::toInteger() const
{
    return get<int>(object);
}

bool RuntimeObject::toBoolean() const
{
    return get<bool>(object);
}

const Array & RuntimeObject::toArray() const
{
    return get<Array>(object);
}

const Object & RuntimeObject::toObject() const
{
    return get<Object>(object);
}

string RuntimeObject::toString(const Heap * heap) const
{
    if (isInteger())
    {
        return to_string(toInteger());
    }
    else if (isBoolean())
    {
        return toBoolean() ? "true" : "false";
    }
    else if (isNull())
    {
        return "null";
    }
    else if (isArray())
    {
        string result = "";
        Array array = get<Array>(object);
        bool first = true;


        for (const auto & iterator : array.data)
        {
            if (first)
            {
                first = false;
            }
            else
            {
                result += ", ";
            }
            RuntimeObject element = heap->operator[](iterator);
            result += element.toString(heap);
        }

        return "[" + result + "]";
    }
    else if (isObject())
    {
        return "()";
    }
    return "";
}

Pointer RuntimeObject::getArrayField(int index) const
{
    if (! isArray())
    {
        throw VirtualMachine::Exception("GetArrayField from unsupported RuntimeObject.");
    }

    Array array = get<Array>(object);
    return array.data[index];
}

void RuntimeObject::setArrayField(int index, Pointer value)
{
    if (! isArray())
    {
        throw VirtualMachine::Exception("SetArrayField from unsupported RuntimeObject.");
    }

    Array array = get<Array>(object);
    array.data[index] = value;
    object = array;
}

Pointer RuntimeObject::getObjectField(const string & name) const
{
    if (! isObject())
    {
        throw VirtualMachine::Exception("GetObjectField from unsupported RuntimeObject.");
    }

    Object o = get<Object>(object);
    for (const auto & it : o.fields)
    {
        if (it.first == name)
        {
            return it.second;
        }
    }
    throw VirtualMachine::Exception("Get undeclared object field " + name + ".");
}

void RuntimeObject::setObjectField(const string & name, Pointer value)
{
    if (! isObject())
    {
        throw VirtualMachine::Exception("SetObjectField from unsupported RuntimeObject.");
    }

    Object o = get<Object>(object);
    for (auto & it : o.fields)
    {
        if (it.first == name)
        {
            it.second = value;
            object = o;
            return;
        }
    }
    throw VirtualMachine::Exception("Set undeclared object field " + name + ".");
}

Pointer RuntimeObject::objectFindMethodReceiver(const std::string & methodName, Pointer currentPointer, const Heap * heap) const
{
    if (! isObject())
    {
        return currentPointer;
    }

    Object o = get<Object>(object);

    for (auto & it : o.methods)
    {
        if (it.first == methodName)
        {
            return currentPointer;
        }
    }

    Pointer parentPointer = o.parent;
    RuntimeObject parent = heap->operator[](parentPointer);
    return parent.objectFindMethodReceiver(methodName, parentPointer, heap);
}

ConstantPoolIndex RuntimeObject::objectFindMethod(const std::string & name) const
{
    if (! isObject())
    {
        throw VirtualMachine::Exception("ObjectFindMethod from unsupported RuntimeObject.");
    }

    Object o = get<Object>(object);
    for (auto & it : o.methods)
    {
        if (it.first == name)
        {
            return it.second;
        }
    }
    throw VirtualMachine::Exception("Unable to find method in object" + name + ".");
}

unsigned int RuntimeObject::size() const
{
    if (isInteger())
    {
        return 5;
    }
    else if (isBoolean())
    {
        return 2;
    }
    else if (isNull())
    {
        return 1;
    }
    else if (isArray())
    {
        int lentgh = get<Array>(object).data.size();
        return 2 * lentgh + 1;
    }
    else
    {
        Object o = get<Object>(object);

        return 1 + 2 + o.fields.size() * 4 + o.methods.size() * 4;
    }
}


/*

RuntimeObject::RuntimeObject(const ProgramObject & programObject)
{


    if (programObject.isInteger())
    {
        programObject.toInteger();
    }
    else if (programObject.isBoolean())
    {
        object = programObject.toBoolean();
    }
    else if (programObject.isNull())
    {
        object = Null();
    }
    else
    {
        throw VirtualMachine::Exception("Creating RuntimeObject from unsupported ProgramObject");
    }
}

*/
