#ifndef POINTER_H
#define POINTER_H

#include <cstring>
#include <string>

class Pointer
{
public:
    Pointer(std::size_t value);
    std::string toString() const;

private:
    friend class Heap;
    friend class GarbageCollector;
    std::size_t value;
};

#endif // POINTER_H
