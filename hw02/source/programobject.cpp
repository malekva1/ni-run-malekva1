#include "programobject.h"

#include <iostream>

#include "instructions.h"
#include "virtualmachine.h"

using namespace std;

#define INTEGER_TAG 0x00
#define BOOLEAN_TAG 0x06
#define NULL_TAG    0x01
#define STRING_TAG  0x02
#define SLOT_TAG    0x04
#define METHOD_TAG  0x03
#define CLASS_TAG   0x05

ProgramObject::ProgramObject(const vector<Byte> & bytecode, const unsigned int startIndex, unsigned int & endIndex, Code * code)
{
   Byte tag = bytecode[startIndex];
   //cout << "creating program object from index " << startIndex << " with tag " << tag  << " = " << hex << (int) tag.toChar() << endl;

   if (tag == INTEGER_TAG)
   {
       value = signedInt32value(bytecode[startIndex + 1],
                                bytecode[startIndex + 2],
                                bytecode[startIndex + 3],
                                bytecode[startIndex + 4]);
       endIndex = startIndex + 5;
       return;
   }
   else if (tag == BOOLEAN_TAG)
   {
       value = (bool)(bytecode[startIndex +1] == 0x01);
       endIndex = startIndex + 2;
       return;
   }
   else if (tag == NULL_TAG)
   {
       value = Unit();
       endIndex = startIndex + 1;
       return;
   }
   else if (tag == STRING_TAG)
   {
       unsigned int stringLength = int32value(bytecode[startIndex + 1],
                                              bytecode[startIndex + 2],
                                              bytecode[startIndex + 3],
                                              bytecode[startIndex + 4]);
       string result;
       result.reserve(stringLength);
       for (unsigned int i = 0; i < stringLength; i++)
       {
           result += bytecode[startIndex + 5 + i].toChar();
       }

       value = result;

       endIndex = startIndex + 5 + stringLength;
       return;
   }
   else if (tag == SLOT_TAG)
   {
       Slot slot;
       slot.value = ConstantPoolIndex(bytecode[startIndex + 1], bytecode[startIndex + 2]);
       value = slot;
       endIndex = startIndex + 3;
       return;
   }
   else if (tag == METHOD_TAG)
   {

       Method method;
       method.nameIndex = ConstantPoolIndex(bytecode[startIndex + 1], bytecode[startIndex + 2]);
       method.arguments = int8value(bytecode[startIndex + 3]);
       method.locals = int16value(bytecode[startIndex + 4], bytecode[startIndex + 5]);
       method.start = code->size();

       value = method;

       unsigned int objectLength = 10;

       unsigned int bodyLength = int32value(bytecode[startIndex + 6],
                                            bytecode[startIndex + 7],
                                            bytecode[startIndex + 8],
                                            bytecode[startIndex + 9]);

       int instructionStart = startIndex + 10;
       for(unsigned int i = 0; i < bodyLength; i++)
       {
           unsigned int instructionLength = getInstructionLength(bytecode[instructionStart]);
           for(unsigned int j = 0; j < instructionLength; j++)
           {
               code->push_back(bytecode[instructionStart + j]);
           }
           objectLength += instructionLength;
           instructionStart += instructionLength;
       }

       endIndex = startIndex + objectLength;
       return;
   }
   else if (tag == CLASS_TAG)
   {
       unsigned int membersLength = int16value(bytecode[startIndex + 1], bytecode[startIndex + 2]);

       Class c;
       for (unsigned int i = 0; i < membersLength; i++)
       {
           c.members.push_back(ConstantPoolIndex(bytecode[startIndex + 2 * i + 3],
                                                 bytecode[startIndex + 2 * i + 4]));
       }

       value = c;
       endIndex = startIndex + 3 + 2 * membersLength;
       return;
   }
   else
   {
       cerr << "Error, creating ProgramObject with unsuported tag " << tag << " (index in bytecode: " << startIndex << ")" << endl;
       throw 1;
   }
}

bool ProgramObject::isInteger() const
{
    return value.index() == 0;
}

bool ProgramObject::isBoolean() const
{
    return value.index() == 1;
}

bool ProgramObject::isNull() const
{
    return value.index() == 2;
}

bool ProgramObject::isString() const
{
    return value.index() == 3;
}

bool ProgramObject::isSlot() const
{
    return value.index() == 4;
}

bool ProgramObject::isMethod() const
{
    return value.index() == 5;
}

bool ProgramObject::isClass() const
{
    return value.index() == 6;
}

int ProgramObject::toInteger() const
{
    if (! isInteger())
    {
        throw VirtualMachine::Exception("Program object not integer toInteger call.");
    }
    return get<int>(value);
}

bool ProgramObject::toBoolean() const
{
    if (! isBoolean())
    {
        throw VirtualMachine::Exception("Program object not boolean toBoolean call.");
    }
    return get<bool>(value);
}

string ProgramObject::toString() const
{
    if (! isString())
    {
        throw VirtualMachine::Exception("Program object not string toString call.");
    }

    return get<string>(value);
}

ConstantPoolIndex ProgramObject::slotValue() const
{
    if (! isSlot())
    {
        throw VirtualMachine::Exception("Program object not slot slotValue call.");
    }

    return get<Slot>(value).value;
}

unsigned int ProgramObject::classMembersCount() const
{
    if (! isClass())
    {
        throw VirtualMachine::Exception("Program object not class classMembersCount call.");
    }
    return get<Class>(value).members.size();
}

ConstantPoolIndex ProgramObject::classMemberIndex(unsigned int index) const
{
    if (! isClass())
    {
        throw VirtualMachine::Exception("Program object not class classMemberIndex call.");
    }

    return get<Class>(value).members[index];
}

ConstantPoolIndex ProgramObject::methodNameIndex() const
{
    if (! isMethod())
    {
        throw VirtualMachine::Exception("Program object not method methodName call.");
    }

    return get<Method>(value).nameIndex;
}

unsigned int ProgramObject::methodStart() const
{
    if (! isMethod())
    {
        throw VirtualMachine::Exception("Program object not method methodStart call.");
    }

    return get<Method>(value).start;
}

unsigned int ProgramObject::methodArguments() const
{
    if (! isMethod())
    {
        throw VirtualMachine::Exception("Program object not method methodArguments call.");
    }

    return get<Method>(value).arguments;
}

unsigned int ProgramObject::methodLocals() const
{
    if (! isMethod())
    {
        throw VirtualMachine::Exception("Program object not method methodLocals call.");
    }

    return get<Method>(value).locals;
}

string ProgramObject::toDebugString() const
{

    if (isInteger())
    {
        return to_string(toInteger());
    }
    else if (isBoolean())
    {
        return toBoolean() ? "true" : "false";
    }
    else if (isNull())
    {
        return "null";
    }
    else if (isString())
    {
        return "\"" + toString() + "\"";
    }
    else if (isSlot())
    {
        return "TAG " + slotValue().toString();
    }
    else if (isMethod())
    {
        return "method, name: " + slotValue().toString();
                + ", #arguments: " + to_string(methodArguments())
                + ", #locals: " + to_string(methodLocals())
                + ", start: " + to_string(methodStart());
    }
    else if (isClass())
    {
        string result;
        for (unsigned int i = 3; i < classMembersCount(); i++)
        {
            result += " "  + classMemberIndex(i).toString();
        }
        return "class" + result;
    }
    return "";
}




