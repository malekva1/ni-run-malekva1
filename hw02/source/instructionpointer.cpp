#include "instructionpointer.h"

#include "instructions.h"
#include "code.h"

using namespace std;

InstructionPointer::InstructionPointer(const Code * code) : code(code), value(0)
{
}

void InstructionPointer::set(size_t newValue)
{
    value = newValue;
}

void InstructionPointer::bump()
{
    Byte opCode = (*code)[*this];
    value += getInstructionLength(opCode);
}

bool InstructionPointer::isValid() const
{
    return value < code->size();
}

InstructionPointer InstructionPointer::operator + (unsigned int n) const
{
    InstructionPointer result(code);
    result.set(value + n);
    return result;
}

string InstructionPointer::toString() const
{
    return to_string(value);
}
