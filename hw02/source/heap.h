#ifndef HEAP_H
#define HEAP_H

#include "pointer.h"
#include "runtimeobject.h"
#include "programobject.h"
#include "heaplogger.h"
#include "garbagecollector.h"

class VirtualMachine;
class Heap
{
public:
    Heap(VirtualMachine * gc);
    ~Heap();

    Pointer newTrivialObject(ProgramObject object);
    Pointer newNull();
    Pointer newBoolen(bool value);
    Pointer newInteger(int value);
    Pointer newArray(int length, Pointer initializerPointer);
    Pointer newObject(Pointer parent, Object::FieldsMap fields, Object::MethodsMap methods);

    const RuntimeObject & operator[](Pointer pointer) const;
    RuntimeObject & operator[](Pointer pointer);

private:
    Pointer addObjectToHeap(RuntimeObject * newObject);

    std::vector<RuntimeObject*> data;
    unsigned int size = 0;
    unsigned int maxSize;

    HeapLoger logger;
    friend class GarbageCollector;

    friend class VirtualMachine;
    GarbageCollector gc;
};

#endif // HEAP_H
