#ifndef CODE_H
#define CODE_H

#include <vector>

#include "byte.h"
#include "instructionpointer.h"

class Code : public std::vector<Byte>
{
public:
    Code() = default;
    void add(Byte byte);

    const Byte & operator [] (const InstructionPointer pointer) const;

    void print(const unsigned int startIndex, unsigned int & endIndex) const;
};

#endif // CODE_H
