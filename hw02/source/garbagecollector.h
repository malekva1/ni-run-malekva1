#ifndef GARBAGECOLLECTOR_H
#define GARBAGECOLLECTOR_H

#include <vector>

#include "pointer.h"

class VirtualMachine;

class GarbageCollector
{
public:
    GarbageCollector(VirtualMachine * vm);
    void collectGarbage();

private:
    void mark();
    void markOperandStack();
    void markFrameStack();
    void markCurrentValues();

    void markObject(Pointer pointer);
    void markArrayMemebers(Pointer pointer);
    void markObjectParentsAndMembers(Pointer pointer);

    void print();

    void sweep();

    std::vector<bool> marked;
    VirtualMachine * vm;
};

#endif // GARBAGECOLLECTOR_H
