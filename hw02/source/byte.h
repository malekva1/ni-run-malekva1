#ifndef BYTE_H
#define BYTE_H

#include <bitset>
#include <iostream>

class Byte //: private std::bitset<8>
{
public:
    Byte() = default;
    Byte(unsigned char x);

    bool operator == (unsigned int val) const;

    unsigned char toChar() const;

    //bool operator [] (std::size_t pos) const;

    friend std::ostream& operator<<(std::ostream& os, Byte b);

private:
    unsigned char value;
};

unsigned int int8value(const Byte first);
unsigned int int16value(const Byte first, const Byte second);
unsigned int int32value(const Byte first, const Byte second, const Byte third, const Byte fourth);

int signedInt32value(const Byte first, const Byte second, const Byte third, const Byte fourth);

#endif // BYTE_H
