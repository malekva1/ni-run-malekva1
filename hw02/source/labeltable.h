#ifndef LABELTABLE_H
#define LABELTABLE_H

#include <map>
#include <string>

#include "instructionpointer.h"

class LabelTable : public std::map<std::string, InstructionPointer>
{
public:
    LabelTable() = default;
    void add(std::string name, InstructionPointer value);
};

#endif // LABELTABLE_H
