#ifndef GLOBALSTABLE_H
#define GLOBALSTABLE_H

#include <map>
#include <string>

class GlobalsTable : public std::map<std::string, unsigned int>
{
public:
    GlobalsTable() = default;
    void add(std::string name, unsigned int index);
};

#endif // GLOBALSTABLE_H
