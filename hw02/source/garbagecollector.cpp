#include "garbagecollector.h"

#include "virtualmachine.h"
#include "runtimeobject.h"

#include <iostream>

using namespace std;

GarbageCollector::GarbageCollector(VirtualMachine * vm)
    : vm(vm)
{}

void GarbageCollector::collectGarbage()
{
    // mark & sweep garbage collection
    mark();
    //print();
    sweep();
}

void GarbageCollector::mark()
{
    marked = vector<bool>(vm->heap.data.size(), false);

    markOperandStack();
    markFrameStack();
    markCurrentValues();
}

void GarbageCollector::markOperandStack()
{
    //cout << "GC: markOperandStack" << endl;

    for ( const auto & it : vm->operandStack.data)
    {
        markObject(it.value);
    }
}

void GarbageCollector::markFrameStack()
{
    //cout << "GC: markFrameStack" << endl;

    for ( const auto & it : vm->frameStack.globalFrame.locals)
    {
        markObject(it.value);
    }

    for (const auto & local : vm->frameStack.localFrames)
    {
        for (const auto & it : local.locals)
        {
            markObject(it.value);
        }
    }
}

void GarbageCollector::markCurrentValues()
{
    //cout << "GC: markCurrentValues" << endl;

    for ( const auto & it : vm->frameValues)
    {
        markObject(it.value);
    }

    if (vm->currentPointer != nullptr)
    {
        markObject(*(vm->currentPointer));
    }
}

void GarbageCollector::markObject(Pointer pointer)
{
    //cout << "GC: mark " << pointer.toString() << endl;

    if (marked[pointer.value])
    {
        return;
    }
    marked[pointer.value] = true;

    auto object = vm->heap[pointer];
    if (object.isArray())
    {
        markArrayMemebers(pointer);
    }
    else if (object.isObject())
    {
        markObjectParentsAndMembers(pointer);
    }
}

void GarbageCollector::markArrayMemebers(Pointer pointer)
{
    auto object = vm->heap[pointer];
    if (! object.isArray())
    {
        throw VirtualMachine::Exception("GC: try to mark members of runtime object that is not array.");
    }

    auto array = object.toArray();

    for (const auto & it : array.data)
    {
        markObject(it);
    }
}

void GarbageCollector::markObjectParentsAndMembers(Pointer pointer)
{
    auto object = vm->heap[pointer];
    if (! object.isObject())
    {
        throw VirtualMachine::Exception("GC: try to mark members of runtime object that is not object.");
    }

    auto o = object.toObject();

    markObject(o.parent);

    for (const auto & it : o.fields)
    {
        markObject(it.second);
    }
}

void GarbageCollector::print()
{
    cout << "GarbageCollector:" << endl;
    for (unsigned int i = 0; i < marked.size(); i++)
    {
        cout << i << ": [" << (marked[i] ? "-" : "x") << "] ";
        if (vm->heap.data[i] == nullptr)
        {
            cout << "--" << endl;
        }
        else
        {
            cout << vm->heap[i].toString(&(vm->heap)) << endl;
        }
    }
}

void GarbageCollector::sweep()
{
    for (unsigned int i = 0; i < marked.size(); i++)
    {
        if (! marked[i])
        {
            if (vm->heap.data[i] == nullptr)
            {
                continue;
            }

            vm->heap.size -= vm->heap.data[i]->size();

            delete vm->heap.data[i];
            vm->heap.data[i] = nullptr;
        }
    }
}
