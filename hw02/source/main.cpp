#include <fstream>
#include <vector>
#include <iostream>

#include "byte.h"
#include "program.h"
#include "virtualmachine.h"

using namespace std;

int heapSizeMB = 128;
bool doHeapLog = false;
string heapLogPath = "";

vector<Byte> readInput(int argc, char ** argv)
{
    for (int i = 0; i < argc; ++i)
    {
        //cerr << argv[i] << endl;
    }

    istream * inputstream;
    bool usingFilestream = false;

    int i = 1;
    while (i < argc)
    {
        if (string(argv[i]) == "--heap-size")
        {
            if (i + 1 >= argc)
            {
                cerr << "Error, no value for --heap-size option" << endl;
                return vector<Byte>();
            }
            heapSizeMB = atoi(argv[i+1]);
            i += 2;
        }
        else if (string(argv[i]) == "--heap-log")
        {
            doHeapLog = true;
            if (i + 1 >= argc)
            {
                cerr << "Error, no value for --heap-log option" << endl;
                return vector<Byte>();
            }
            heapLogPath = argv[i+1];

            i += 2;
        }
        else
        {
            ifstream * fileStream = new ifstream;
            fileStream->open(argv[i], ios::in | ios::binary);
            inputstream = fileStream;
            usingFilestream = true;

            i++;
        }
    }

    if (! usingFilestream)
    {
        inputstream = &cin;
    }

    vector<Byte> result;

    char x;
    while(true)
    {
        inputstream->read(&x,1);
        if (! inputstream->good())
        {
            break;
        }
        result.push_back(Byte(x));
    }

    if (usingFilestream)
    {
        delete inputstream;
    }

    return result;
}

int main(int argc, char ** argv)
{

    Program program(readInput(argc, argv));

    try
    {
        VirtualMachine::run(program);
    }
    catch (VirtualMachine::Exception ex)
    {
        cerr << "VM Error: " << ex.message << endl;
        return 1;
    }

    return 0;
}
