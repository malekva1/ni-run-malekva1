#ifndef INSTRUCTIONPOINTER_H
#define INSTRUCTIONPOINTER_H

#include <string>

class Code;

class InstructionPointer
{
public:
    InstructionPointer(const Code * code);

    void set(std::size_t newValue);

    void bump();
    bool isValid() const;

    InstructionPointer operator + (unsigned int n) const;

    std::string toString() const;


    std::size_t value;
private:
    friend class Code;
    const Code * code;
};

#endif // INSTRUCTIONPOINTER_H
