#include "functiontable.h"

using namespace std;

void FunctionTable::add(string name, ConstantPoolIndex index)
{
    map<string, ConstantPoolIndex>::insert(make_pair(name, index));
}
