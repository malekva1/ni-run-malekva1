#include "code.h"

#include <iostream>

#include "instructions.h"

using namespace std;

void Code::add(Byte byte)
{
    push_back(byte);
}

const Byte & Code::operator [] (const InstructionPointer pointer) const
{
    return vector<Byte>::operator[](pointer.value);
}


void Code::print(const unsigned int startIndex, unsigned int & endIndex) const
{
    cout <<startIndex << ":\t" << printInstruction(*this, startIndex, endIndex) << endl;
}
