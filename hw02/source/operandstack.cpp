#include "operandstack.h"

#include <iostream>

#include "heap.h"

using namespace std;

void OperandStack::print(Heap * heap) const
{
    cout << endl << "OPERAND STACKT:" << endl;

    for (const auto & it : data)
    {
        cout << " - " << it.toString() << " = " << heap->operator[](it).toString(heap)<< endl;
    }

    cout << endl;
}

void OperandStack::push(Pointer pointer)
{
    data.push_back(pointer);
}

Pointer OperandStack::top() const
{
    return data.back();
}

void OperandStack::pop()
{
    data.pop_back();
}

bool OperandStack::empty() const
{
    return data.empty();
}
