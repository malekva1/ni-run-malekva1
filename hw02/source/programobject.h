#ifndef PROGRAMOBJECT_H
#define PROGRAMOBJECT_H

#include <vector>
#include <string>
#include <variant>

#include "byte.h"
#include "code.h"
#include "constantpoolindex.h"

class ProgramObject
{
public:
    ProgramObject(const std::vector<Byte> & bytecode, const unsigned int startIndex, unsigned int & endIndex, Code * code);

    bool isInteger() const;
    bool isBoolean() const;
    bool isNull() const;
    bool isString() const;
    bool isSlot() const;
    bool isMethod() const;
    bool isClass() const;

    int toInteger() const;
    bool toBoolean() const;
    std::string toString() const;

    ConstantPoolIndex slotValue() const;

    ConstantPoolIndex methodNameIndex() const;
    unsigned int methodStart() const;
    unsigned int methodArguments() const;
    unsigned int methodLocals() const;

    unsigned int classMembersCount() const;
    ConstantPoolIndex classMemberIndex(unsigned int index) const;

    std::string toDebugString() const;

private:
    struct Unit{};
    struct Slot
    {
        ConstantPoolIndex value;
    };
    struct Method
    {
        ConstantPoolIndex nameIndex;
        unsigned int start;
        unsigned int arguments;
        unsigned int locals;
    };
    struct Class
    {
        std::vector<ConstantPoolIndex> members;
    };

    std::variant<int, bool, Unit, std::string, Slot, Method, Class> value;
};

#endif // PROGRAMOBJECT_H
