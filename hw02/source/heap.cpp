#include "heap.h"


using namespace std;

extern int heapSizeMB;

Heap::Heap(VirtualMachine * vm)
    : gc(vm)
{
    logger.addStart();
    maxSize = 1024 * 1024 * heapSizeMB;
}

Heap::~Heap()
{
    logger.writeLog();
    for (auto it : data)
    {
        if (it != nullptr)
        {
            delete it;
        }
    }
}

Pointer Heap::newTrivialObject(ProgramObject object)
{
    RuntimeObject * newObject = new RuntimeObject(object);
    return addObjectToHeap(newObject);
}

Pointer Heap::newNull()
{
    RuntimeObject * newObject = new RuntimeObject();
    return addObjectToHeap(newObject);
}

Pointer Heap::newBoolen(bool value)
{
    RuntimeObject * newObject = new RuntimeObject(value);
    return addObjectToHeap(newObject);
}

Pointer Heap::newInteger(int value)
{
    RuntimeObject * newObject = new RuntimeObject(value);
    return addObjectToHeap(newObject);
}

Pointer Heap::newArray(int length, Pointer initializerPointer)
{
    RuntimeObject * newObject = new RuntimeObject(length, initializerPointer);
    return addObjectToHeap(newObject);
}

Pointer Heap::newObject(Pointer parent, Object::FieldsMap fields, Object::MethodsMap methods)
{
    RuntimeObject * newObject = new RuntimeObject(parent, fields, methods);
    return addObjectToHeap(newObject);
}

Pointer Heap::addObjectToHeap(RuntimeObject * newObject)
{
    Pointer pointer(data.size());

    unsigned int newSize = size + newObject->size();
    if (newSize > maxSize)
    {
        gc.collectGarbage();
        newSize = size + newObject->size();
        logger.addGC(size);
    }

    data.push_back(newObject);
    size = newSize;
    logger.addAllocation(size);

    //cout << "HEAP: new" << newObject->toString(this) << endl;

    return pointer;
}

const RuntimeObject & Heap::operator[](Pointer pointer) const
{
    return *data[pointer.value];
}

RuntimeObject & Heap::operator[](Pointer pointer)
{
    return *data[pointer.value];
}
