#ifndef GLOBALS_H
#define GLOBALS_H

#include <vector>

#include "constantpoolindex.h"

class Globals : public std::vector<ConstantPoolIndex>
{
public:
    Globals() = default;
    void add(ConstantPoolIndex index);

    void print() const;
};

#endif // GLOBALS_H
