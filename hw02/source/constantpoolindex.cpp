#include "constantpoolindex.h"

using namespace std;

ConstantPoolIndex::ConstantPoolIndex(Byte first, Byte second): value(int16value(first, second))
{
}

string ConstantPoolIndex::toString() const
{
    return "#" + to_string(value);
}


