#ifndef CONSTANTPOOL_H
#define CONSTANTPOOL_H

#include <vector>

#include "programobject.h"
#include "constantpoolindex.h"

class ConstantPool
{
public:
    ConstantPool() = default;
    void add(ProgramObject object);

    const ProgramObject & operator[] (const ConstantPoolIndex & index) const;

    void print() const;

private:
    std::vector<ProgramObject> data;
};

#endif // CONSTANTPOOL_H
