#include "labeltable.h"

using namespace std;

void LabelTable::add(std::string name, InstructionPointer value)
{
    map<string, InstructionPointer>::insert(make_pair(name, value));
}
