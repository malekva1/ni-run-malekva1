#ifndef INSTRUCTIONS_H
#define INSTRUCTIONS_H

#include <vector>

#include "byte.h"

#define LABEL_OPCODE            0x00
#define LITERAL_OPCODE          0x01
#define PRINT_OPCODE            0x02
#define ARRAY_OPCODE            0x03
#define OBJECT_OPCODE           0x04
#define GETSLOT_OPCODE          0x05
#define SETSLOT_OPCODE          0x06
#define CALLMETHOD_OPCODE       0x07
#define CALLFUNCTION_OPCODE     0x08
#define SETLOCAL_OPCODE         0x09
#define GETLOCAL_OPCODE         0x0A
#define SETGLOBAL_OPCODE        0x0B
#define GETGLOBAL_OPCODE        0x0C
#define BRANCH_OPCODE           0x0D
#define JUMP_OPCODE             0x0E
#define RETURN_OPCODE           0x0F
#define DROP_OPCODE             0x10

unsigned int getInstructionLength(Byte opCode);

std::string printInstruction(const std::vector<Byte> code, const unsigned int startIndex, unsigned int & endIndex);

#endif // INSTRUCTIONS_H
