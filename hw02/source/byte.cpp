#include "byte.h"

Byte::Byte(unsigned char x) //: bitset(x)
{
    value = x;
}

bool Byte::operator == (unsigned int val) const
{
    return (char)val == value;
}

unsigned char Byte::toChar() const
{
    return value;
}

std::ostream& operator<<(std::ostream& os, Byte b)
{
    os << b.value;
    return os;
}

unsigned int int8value(const Byte first)
{

    unsigned int result = first.toChar();

    return result;
}

unsigned int int16value(const Byte first, const Byte second)
{
   unsigned int result = 0;
   result += int8value(first);
   result += 256 * int8value(second);
   return result;
}

unsigned int int32value(const Byte first, const Byte second, const Byte third, const Byte fourth)
{
   unsigned int result = 0;
   result += int8value(first);
   result += 256 * int8value(second);
   result += 65536 * int8value(third);
   result += 16777216 * int8value(fourth);
   return result;
}

int signedInt32value(const Byte first, const Byte second, const Byte third, const Byte fourth)
{
    Byte bytes [4] = {first, second, third, fourth};
    int result = 0;
    for (int i = 0; i < 4; i++)
    {
        for(int j = 0; j < 8; j++)
        {
            if((bytes[i].toChar() & (1 << j)) > 0)
            {
                result = result | (1 << (8 * i + j));
            }
        }
    }
    return result;
}
