#include "instructions.h"

#include <iostream>
#include <string>

using namespace std;

unsigned int getInstructionLength(Byte opCode)
{
    if (opCode == LABEL_OPCODE)
    {
        return 3;
    }
    else if (opCode == LITERAL_OPCODE)
    {
        return 3;
    }
    else if (opCode == PRINT_OPCODE)
    {
        return 4;
    }
    else if (opCode == ARRAY_OPCODE)
    {
        return 1;
    }
    else if (opCode == OBJECT_OPCODE)
    {
        return 3;
    }
    else if (opCode == GETSLOT_OPCODE)
    {
        return 3;
    }
    else if (opCode == SETSLOT_OPCODE)
    {
        return 3;
    }
    else if (opCode == CALLMETHOD_OPCODE)
    {
        return 4;
    }
    else if (opCode == CALLFUNCTION_OPCODE)
    {
        return 4;
    }
    else if (opCode == SETLOCAL_OPCODE)
    {
        return 3;
    }
    else if (opCode == GETLOCAL_OPCODE)
    {
        return 3;
    }
    else if (opCode == SETGLOBAL_OPCODE)
    {
        return 3;
    }
    else if (opCode == GETGLOBAL_OPCODE)
    {
        return 3;
    }
    else if (opCode == BRANCH_OPCODE)
    {
        return 3;
    }
    else if (opCode == JUMP_OPCODE)
    {
        return 3;
    }
    else if (opCode == RETURN_OPCODE)
    {
        return 1;
    }
    else if (opCode == DROP_OPCODE)
    {
        return 1;
    }
    else
    {
        cerr << "getInstructionLength: Unsupported instruction, opcode " << (int8value(opCode)) << "." << endl;
    }
    return -1;
}

string printInstruction(const vector<Byte> code, const unsigned int startIndex, unsigned int & endIndex)
{
    Byte opCode = code[startIndex];
    endIndex = startIndex + getInstructionLength(opCode);

    if (opCode == LABEL_OPCODE)
    {
        unsigned int index = int16value(code[startIndex + 1], code[startIndex + 2]);
        return "label #" + to_string(index);
    }
    else if (opCode == LITERAL_OPCODE)
    {
        unsigned int index = int16value(code[startIndex + 1], code[startIndex + 2]);
        return "literal #" + to_string(index);
    }
    else if (opCode == PRINT_OPCODE)
    {
        unsigned int index = int16value(code[startIndex + 1], code[startIndex + 2]);
        unsigned int arguments = int8value(code[startIndex + 3]);
        return "print format #" + to_string(index) + " #arguments: " + to_string(arguments);
    }
    else if (opCode == ARRAY_OPCODE)
    {
        return "array";
    }
    else if (opCode == OBJECT_OPCODE)
    {
        unsigned int index = int16value(code[startIndex + 1], code[startIndex + 2]);
        return "object #" + to_string(index);
    }
    else if (opCode == GETSLOT_OPCODE)
    {
        unsigned int index = int16value(code[startIndex + 1], code[startIndex + 2]);
        return "get slot #" + to_string(index);
    }
    else if (opCode == SETSLOT_OPCODE)
    {
        unsigned int index = int16value(code[startIndex + 1], code[startIndex + 2]);
        return "set slot #" + to_string(index);
    }
    else if (opCode == CALLMETHOD_OPCODE)
    {
        unsigned int index = int16value(code[startIndex + 1], code[startIndex + 2]);
        unsigned int arguments = int8value(code[startIndex + 3]);
        return "call method name #" + to_string(index) + " #arguments: " + to_string(arguments);
    }
    else if (opCode == CALLFUNCTION_OPCODE)
    {
        unsigned int index = int16value(code[startIndex + 1], code[startIndex + 2]);
        unsigned int arguments = int8value(code[startIndex + 3]);
        return "call function name #" + to_string(index) + " #arguments: " + to_string(arguments);
    }
    else if (opCode == SETLOCAL_OPCODE)
    {
        unsigned int index = int16value(code[startIndex + 1], code[startIndex + 2]);
        return "set local #" + to_string(index);
    }
    else if (opCode == GETLOCAL_OPCODE)
    {
        unsigned int index = int16value(code[startIndex + 1], code[startIndex + 2]);
        return "get local #" + to_string(index);
    }
    else if (opCode == SETGLOBAL_OPCODE)
    {
        unsigned int index = int16value(code[startIndex + 1], code[startIndex + 2]);
        return "set global #" + to_string(index);
    }
    else if (opCode == GETGLOBAL_OPCODE)
    {
        unsigned int index = int16value(code[startIndex + 1], code[startIndex + 2]);
        return "get global #" + to_string(index);
    }
    else if (opCode == BRANCH_OPCODE)
    {
        unsigned int index = int16value(code[startIndex + 1], code[startIndex + 2]);
        return "branch #" + to_string(index);
    }
    else if (opCode == JUMP_OPCODE)
    {
        unsigned int index = int16value(code[startIndex + 1], code[startIndex + 2]);
        return "jump #" + to_string(index);
    }
    else if (opCode == RETURN_OPCODE)
    {
        return "return";
    }
    else if (opCode == DROP_OPCODE)
    {
        return "drop";
    }
    else
    {
        cerr << "getInstructionLength: Unsupported instruction, opcode " << (int8value(opCode)) << "." << endl;
        throw 1;
    }

    return "";
}
