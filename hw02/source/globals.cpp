#include "globals.h"

#include <iostream>

using namespace std;

void Globals::add(ConstantPoolIndex index)
{
    vector<ConstantPoolIndex>::push_back(index);
}

void Globals::print() const
{
    for (const auto & it : *this)
    {
        cout << it.toString() << endl;
    }
}
