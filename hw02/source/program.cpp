#include "program.h"

#include <iostream>

#include "programobject.h"

using namespace std;

Program::Program(vector<Byte> bytecode)
{
    // cout << "creating program " << bytecode.size() << endl;

    unsigned int bytecodeIndex = 0;

    // constantPool and code
    unsigned int constantPoolLength = int16value(bytecode[0], bytecode[1]);
    bytecodeIndex += 2;

    for (unsigned int i = 0; i < constantPoolLength; i++)
    {
        constantPool.add(ProgramObject(bytecode, bytecodeIndex, bytecodeIndex, &code));
    }

    // globals
    unsigned int globalsLength = int16value(bytecode[bytecodeIndex], bytecode[bytecodeIndex + 1]);
    bytecodeIndex += 2;

    for (unsigned int i = 0; i < globalsLength; i++)
    {
        globals.add(ConstantPoolIndex(bytecode[bytecodeIndex], bytecode[bytecodeIndex + 1]));
        bytecodeIndex += 2;
    }

    // entryPoint
    entryPoint = ConstantPoolIndex(bytecode[bytecodeIndex], bytecode[bytecodeIndex + 1]);
    bytecodeIndex += 2;
}

const ConstantPool & Program::getConstantPool() const
{
    return constantPool;
}

const Globals & Program::getGlobals() const
{
    return globals;
}

const ConstantPoolIndex & Program::getEntryPoint() const
{
    return entryPoint;
}

const Code & Program::getCode() const
{
    return code;
}

void Program::print() const
{
    cout << "Constant Pool:" << endl;
    constantPool.print();
    cout << endl;

    cout << "Globals:" << endl;
    globals.print();
    cout << endl;

    cout << "Entry Point:" << endl;
    cout << entryPoint.toString() << endl;
    cout << endl;

    cout << "Code:" << endl;
    unsigned int i = 0;
    while (i < code.size())
    {
        code.print(i, i);
    }
    cout << endl;
}
