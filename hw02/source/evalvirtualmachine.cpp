#include "virtualmachine.h"

#include <iostream>
#include <string>
#include <list>
#include <stack>

#include "pointer.h"

using namespace std;

void VirtualMachine::evalLabel()
{
    instructionPointer.bump();
}

void VirtualMachine::evalLiteral()
{
    ConstantPoolIndex index(code(instructionPointer + 1),
                            code(instructionPointer + 2));

    ProgramObject object = constantPool(index);

    if (! (object.isInteger() || object.isBoolean() || object.isNull()))
    {
        throw Exception("Literal with unsupported object, instruction pointer: " + instructionPointer.toString() + ".");
    }

    Pointer pointer = heap.newTrivialObject(object);
    operandStack.push(pointer);

    instructionPointer.bump();
    //cerr << "literal " << object.toDebugString() << endl;
}

void VirtualMachine::evalPrint()
{

    ConstantPoolIndex formatStringIndex(code(instructionPointer + 1),
                                        code(instructionPointer + 2));

    if (! constantPool(formatStringIndex).isString())
    {
        throw Exception("Print format string is not a string.");
    }

    string formatString = constantPool(formatStringIndex).toString();

    unsigned int numberOfArguments = int8value(code(instructionPointer + 3));

    list<Pointer> argumentPointers;
    for (unsigned int i = 0; i < numberOfArguments; i++)
    {
        if(operandStack.empty())
        {
            throw Exception("Print argument from empty stack, instruction pointer: " + instructionPointer.toString() + ".");
        }
        argumentPointers.push_front(operandStack.top());
        operandStack.pop();
    }

    //int printedArguments = 0;
    for (unsigned int i = 0; i < formatString.length(); i++)
    {
        char ch = formatString[i];
        if (ch == '\\')
        {
            if (i == formatString.length() - 1)
            {
                throw Exception("Print with invalid format string, \\ is last character.");
            }

            char nextCh = formatString[i+1];

            if (nextCh == '~')
            {
                cout << "~";
            }
            else if (nextCh == 'n')
            {
                cout << endl;
            }
            else if (nextCh == '"')
            {
                cout << "\"";
            }
            else if (nextCh == 'r')
            {
                cout << "\r";
            }
            else if (nextCh == 't')
            {
                cout << "\t";
            }
            else if (nextCh == '\\')
            {
                cout << "\\";
            }
            else
            {
                throw Exception("Print with invalid format string, \\ can not be followed by \"" + string(1, nextCh) + "\".");
            }
            i++;
            continue;
        }
        else if (ch == '~')
        {
            Pointer pointer = argumentPointers.front();
            argumentPointers.pop_front();
            cout << heap[pointer].toString(&heap);
        }
        else
        {
            cout << ch;
        }
    }

    Pointer pointer = heap.newNull();
    operandStack.push(pointer);

    instructionPointer.bump();
}

void VirtualMachine::evalArray()
{
    Pointer initializerPointer = operandStack.top();
    currentPointer = &initializerPointer;
    operandStack.pop();
    Pointer lengthPointer = operandStack.top();
    operandStack.pop();

    if (! heap[lengthPointer].isInteger())
    {
        throw Exception("Array size is not a integer, instruction pointer: " + instructionPointer.toString() + ".");
    }

    int length = heap[lengthPointer].toInteger();
    if (length < 0)
    {
        throw Exception("Array size is negative integer, instruction pointer: " + instructionPointer.toString() + ".");
    }

    Pointer arrayPointer = heap.newArray(length, initializerPointer);
    currentPointer = nullptr;

    operandStack.push(arrayPointer);

    instructionPointer.bump();
}

void VirtualMachine::evalObject()
{
    ConstantPoolIndex index(code(instructionPointer + 1),
                            code(instructionPointer + 2));

    ProgramObject object = constantPool(index);
    if (! object.isClass())
    {
        throw Exception("Object is not associated with class, instruction pointer: " + instructionPointer.toString() + ".");
    }

    stack<string> fieldsNames;
    Object::MethodsMap methods;

    unsigned int membersCount = object.classMembersCount();

    for (unsigned int i = 0; i < membersCount; i++)
    {
        ConstantPoolIndex memberIndex = object.classMemberIndex(i);
        ProgramObject member = constantPool(memberIndex);

        if (member.isSlot())
        {
            ConstantPoolIndex nameIndex = member.slotValue();
            ProgramObject nameObject = constantPool(nameIndex);
            if (! nameObject.isString())
            {
                throw Exception("Slot name is not a string, instruction pointer: " + instructionPointer.toString() + ".");
            }
            string name = nameObject.toString();
            fieldsNames.push(name);
        }
        else if (member.isMethod())
        {
             ConstantPoolIndex nameIndex = member.methodNameIndex();
             ProgramObject nameObject = constantPool(nameIndex);
             if (! nameObject.isString())
             {
                 throw Exception("Method name is not a string, instruction pointer: " + instructionPointer.toString() + ".");
             }
             string name = nameObject.toString();
             methods.push_back(make_pair(name, memberIndex));
        }
        else
        {
            throw Exception("Object member is not slot nor method, instruction pointer: " + instructionPointer.toString() + ".");
        }
    }

    Object::FieldsMap fields;
    frameValues.clear();
    while(! fieldsNames.empty())
    {
        string name = fieldsNames.top();
        fieldsNames.pop();

        Pointer value = operandStack.top();
        operandStack.pop();

        fields.push_back(make_pair(name, value));
        frameValues.push_back(value);
    }

    Pointer parent = operandStack.top();
    currentPointer = &parent;
    operandStack.pop();

    Pointer objectPointer = heap.newObject(parent, fields, methods);
    currentPointer = nullptr;
    frameValues.clear();

    operandStack.push(objectPointer);

    instructionPointer.bump();
}

void VirtualMachine::evalGetSlot()
{
    ConstantPoolIndex nameIndex(code(instructionPointer + 1),
                                code(instructionPointer + 2));

    ProgramObject programObject = constantPool(nameIndex);
    if (! programObject.isString())
    {
        throw Exception("GetSlot name is not string, instruction pointer: " + instructionPointer.toString() + ".");
    }
    string name = programObject.toString();

    Pointer pointer = operandStack.top();
    operandStack.pop();

    RuntimeObject object = heap[pointer];

    if (! object.isObject())
    {
        throw Exception("GetSlot object is not object, instruction pointer: " + instructionPointer.toString() + ".");
    }

    Pointer result = object.getObjectField(name);
    operandStack.push(result);

    instructionPointer.bump();
}

void VirtualMachine::evalSetSlot()
{
    ConstantPoolIndex nameIndex(code(instructionPointer + 1),
                                code(instructionPointer + 2));

    ProgramObject programObject = constantPool(nameIndex);
    if (! programObject.isString())
    {
        throw Exception("GetSlot name is not string, instruction pointer: " + instructionPointer.toString() + ".");
    }
    string name = programObject.toString();

    Pointer value = operandStack.top();
    operandStack.pop();

    Pointer pointer = operandStack.top();
    operandStack.pop();

    RuntimeObject object = heap[pointer];

    if (! object.isObject())
    {
        throw Exception("GetSlot object is not object, instruction pointer: " + instructionPointer.toString() + ".");
    }

    object.setObjectField(name, value);
    heap[pointer] = object;

    operandStack.push(value);

    instructionPointer.bump();
}

void VirtualMachine::evalCallMethod()
{
    ConstantPoolIndex nameIndex(code(instructionPointer + 1),
                                code(instructionPointer + 2));

    ProgramObject nameObject = constantPool(nameIndex);
    if (! nameObject.isString())
    {
        throw Exception("CallMethod name is not string, instruction pointer: " + instructionPointer.toString() + ".");
    }
    string name = nameObject.toString();

    unsigned int argumentsCount = int8value(code(instructionPointer + 3));

    stack<Pointer> tmpStack;
    for (unsigned int i = 0; i < argumentsCount - 1; i++)
    {
        tmpStack.push(operandStack.top());
        operandStack.pop();
    }

    Pointer receiverPointer = operandStack.top();
    operandStack.pop();

    RuntimeObject receiver = heap[receiverPointer];

    if (receiver.isObject())
    {
        receiverPointer = receiver.objectFindMethodReceiver(name, receiverPointer, &heap);
        receiver = heap[receiverPointer];

        if (receiver.isObject())
        {
             ConstantPoolIndex methodIndex = receiver.objectFindMethod(name);
             ProgramObject methodObject = constantPool(methodIndex);
             if (! methodObject.isMethod())
             {
                 throw Exception("CallMethod object is not method, instruction pointer: " + instructionPointer.toString() + ".");
             }
             if (argumentsCount != methodObject.methodArguments())
             {
                 throw Exception("Function count of arguments does not match, instruction pointer: " + instructionPointer.toString() + ".");
             }

             frameValues.clear();
             frameValues.push_back(receiverPointer);

             while(! tmpStack.empty())
             {
                 frameValues.push_back(tmpStack.top());
                 tmpStack.pop();
             }

             for (unsigned int i = 0; i < methodObject.methodLocals(); i++)
             {
                 frameValues.push_back(heap.newNull());
             }

             instructionPointer.bump();

             frameStack.push(instructionPointer, frameValues);
             frameValues.clear();

             instructionPointer.set(methodObject.methodStart());
             return;
        }
    }

    if (receiver.isNull())
    {
        if (argumentsCount != 2)
        {
            throw Exception("CallMethod on null has wrong number of arguments, instruction pointer: " + instructionPointer.toString() + ".");
        }

        RuntimeObject argument = heap[tmpStack.top()];

        bool result = false;
        if (name == "eq" || name == "==")
        {
            result = argument.isNull();
        }
        else if (name == "neq" || name == "!=")
        {
            result = ! argument.isNull();
        }
        else
        {
            throw Exception("CallMethod on null (possible parent of some object) with wrong name \"" + name + "\", instruction pointer: " + instructionPointer.toString() + ".");
        }

        Pointer resultPointer = heap.newBoolen(result);
        operandStack.push(resultPointer);
        instructionPointer.bump();
    }
    else if (receiver.isInteger())
    {
        if (argumentsCount != 2)
        {
            throw Exception("CallMethod on integer has wrong number of arguments, instruction pointer: " + instructionPointer.toString() + ".");
        }

        int value = receiver.toInteger();

        RuntimeObject argument = heap[tmpStack.top()];
        if (name == "eq" || name == "==" || name == "neq" || name == "!=")
        {
            bool result = false;
            if (name == "eq" || name == "==")
            {
                if (argument.isInteger())
                {
                    result = value == argument.toInteger();
                }
                else
                {
                    result = false;
                }

            }
            else if (name == "neq" || name == "!=")
            {
                if (argument.isInteger())
                {
                    result = value != argument.toInteger();
                }
                else
                {
                    result = true;
                }
            }
            Pointer resultPointer = heap.newBoolen(result);
            operandStack.push(resultPointer);
        }
        else if(name == "+" || name == "add" || name == "-" || name == "sub" || name == "*" || name == "mul" || name == "/" || name == "div" || name == "%" || name == "mod")
        {
            if (! argument.isInteger())
            {
                throw Exception("CallMethod with name \"" + name + "\"on integer with argument not integer, instruction pointer: " + instructionPointer.toString() + ".");
            }

            int argumentValue = argument.toInteger();
            int result = 0;
            if(name == "+" || name == "add")
            {
                result = value + argumentValue;
            }
            else if (name == "-" || name == "sub")
            {
                result = value - argumentValue;
            }
            else if (name == "*" || name == "mul")
            {
                result = value * argumentValue;
            }
            else if (name == "/" || name == "div")
            {
                result = value / argumentValue;
            }
            else if (name == "%" || name == "mod")
            {
                result = value % argumentValue;
            }

            Pointer resultPointer = heap.newInteger(result);
            operandStack.push(resultPointer);
        }
        else if(name == "<=" || name == "le" || name == ">=" || name == "ge" || name == "<" || name == "lt" || name == ">" || name == "gt")
        {
            if (! argument.isInteger())
            {
                throw Exception("CallMethod with name \"" + name + "\"on integer with argument not integer, instruction pointer: " + instructionPointer.toString() + ".");
            }

            int argumentValue = argument.toInteger();
            bool result = false;

            if(name == "<=" || name == "le")
            {
                result = value <= argumentValue;
            }
            else if (name == ">=" || name == "ge")
            {
                result = value >= argumentValue;
            }
            else if (name == "<" || name == "lt")
            {
                result = value < argumentValue;
            }
            else if (name == ">" || name == "gt")
            {
                result = value > argumentValue;
            }

            Pointer resultPointer = heap.newBoolen(result);
            operandStack.push(resultPointer);
        }
        else
        {
            throw Exception("CallMethod on integer with wrong name \"" + name + "\", instruction pointer: " + instructionPointer.toString() + ".");
        }
        instructionPointer.bump();
    }
    else if (receiver.isBoolean())
    {
        if (argumentsCount != 2)
        {
            throw Exception("CallMethod on boolean has wrong number of arguments, instruction pointer: " + instructionPointer.toString() + ".");
        }

        bool value = receiver.toBoolean();

        RuntimeObject argument = heap[tmpStack.top()];
        if (name == "eq" || name == "==" || name == "neq" || name == "!=")
        {
            bool result = false;
            if (name == "eq" || name == "==")
            {
                if (argument.isBoolean())
                {
                    result = value == argument.toBoolean();
                }
                else
                {
                    result = false;
                }
            }
            else if (name == "neq" || name == "!=")
            {
                if (argument.isBoolean())
                {
                    result = value != argument.toBoolean();
                }
                else
                {
                    result = true;
                }
            }
            Pointer resultPointer = heap.newBoolen(result);
            operandStack.push(resultPointer);
        }
        else if(name == "&" || name == "and" || name == "|" || name == "or")
        {
            if (! argument.isBoolean())
            {
                throw Exception("CallMethod with name \"" + name + "\"on boolean with argument not boolean, instruction pointer: " + instructionPointer.toString() + ".");
            }

            bool argumentValue = argument.toBoolean();
            bool result = false;

            if(name == "&" || name == "and")
            {
                result = value && argumentValue;
            }
            else if (name == "|" || name == "or")
            {
                result = value || argumentValue;
            }

            Pointer resultPointer = heap.newBoolen(result);
            operandStack.push(resultPointer);
        }
        else
        {
            throw Exception("CallMethod on integer with wrong name \"" + name + "\", instruction pointer: " + instructionPointer.toString() + ".");
        }
        instructionPointer.bump();
    }
    else if (receiver.isArray())
    {
        if (name == "get")
        {
            if (argumentsCount != 2)
            {
                throw Exception("CallMethod get on array has wrong number of arguments, instruction pointer: " + instructionPointer.toString() + ".");
            }

            RuntimeObject argument = heap[tmpStack.top()];
            if (! argument.isInteger())
            {
                throw Exception("CallMethod get on array has invalid type of argument (not ingeter), instruction pointer: " + instructionPointer.toString() + ".");
            }
            int index = argument.toInteger();

            operandStack.push(receiver.getArrayField(index));
        }
        else if (name == "set")
        {
            if (argumentsCount != 3)
            {
                throw Exception("CallMethod set on array has wrong number of arguments, instruction pointer: " + instructionPointer.toString() + ".");
            }

            RuntimeObject argumentIndex = heap[tmpStack.top()];
            tmpStack.pop();

            if (! argumentIndex.isInteger())
            {
                throw Exception("CallMethod set on array has invalid type of index argument (not ingeter), instruction pointer: " + instructionPointer.toString() + ".");
            }
            int index = argumentIndex.toInteger();

            Pointer valuePointer = tmpStack.top();

            heap[receiverPointer].setArrayField(index, valuePointer);
            operandStack.push(valuePointer);

        }
        else
        {
            throw Exception("CallMethod on array with wrong name \"" + name + "\", instruction pointer: " + instructionPointer.toString() + ".");
        }
        instructionPointer.bump();
    }
}

void VirtualMachine::evalCallFunction()
{
    ConstantPoolIndex nameIndex(code(instructionPointer + 1),
                                code(instructionPointer + 2));

    ProgramObject nameObject = constantPool(nameIndex);
    if (! nameObject.isString())
    {
        throw Exception("CallFunction name is not string, instruction pointer: " + instructionPointer.toString() + ".");
    }
    string name = nameObject.toString();
    if (functionTable.find(name) == functionTable.end())
    {
        throw Exception("Calling nonexistent function " + name + ", instruction pointer: " + instructionPointer.toString() + ".");
    }

    ConstantPoolIndex methodIndex = functionTable[nameObject.toString()];
    ProgramObject methodObject = constantPool(methodIndex);
    if (! methodObject.isMethod())
    {
        throw Exception("CallFunction object is not method, instruction pointer: " + instructionPointer.toString() + ".");
    }

    unsigned int argumentsCount = int8value(code(instructionPointer + 3));
    if (argumentsCount != methodObject.methodArguments())
    {
        throw Exception("Function count of arguments does not match, instruction pointer: " + instructionPointer.toString() + ".");
    }

    stack<Pointer> tmpStack;
    for (unsigned int i = 0; i < argumentsCount; i++)
    {
        tmpStack.push(operandStack.top());
        operandStack.pop();
    }

    frameValues.clear();

    while(! tmpStack.empty())
    {
        frameValues.push_back(tmpStack.top());
        tmpStack.pop();
    }

    for (unsigned int i = 0; i < methodObject.methodLocals(); i++)
    {
        frameValues.push_back(heap.newNull());
    }

    instructionPointer.bump();

    frameStack.push(instructionPointer, frameValues);
    frameValues.clear();

    instructionPointer.set(methodObject.methodStart());
}

void VirtualMachine::evalSetLocal()
{
    unsigned int index = int16value(code(instructionPointer + 1),
                                    code(instructionPointer + 2));

    Pointer pointer = operandStack.top();
    frameStack.setLocal(index, pointer);

    instructionPointer.bump();
}

void VirtualMachine::evalGetLocal()
{

    unsigned int index = int16value(code(instructionPointer + 1),
                                    code(instructionPointer + 2));

    Pointer pointer = frameStack.local(index);
    operandStack.push(pointer);

    instructionPointer.bump();
}

void VirtualMachine::evalSetGlobal()
{
    ConstantPoolIndex nameIndex(code(instructionPointer + 1),
                                code(instructionPointer + 2));

    ProgramObject object = constantPool(nameIndex);
    if (! object.isString())
    {
        throw Exception("SetGlobal name is not string, instruction pointer: " + instructionPointer.toString() + ".");
    }
    string name = object.toString();
    unsigned int index = globalsTable.find(name)->second;

    Pointer value = operandStack.top();

    frameStack.setGlobal(index, value);

    instructionPointer.bump();
}

void VirtualMachine::evalGetGlobal()
{
    ConstantPoolIndex nameIndex(code(instructionPointer + 1),
                                code(instructionPointer + 2));

    ProgramObject object = constantPool(nameIndex);
    if (! object.isString())
    {
        throw Exception("SetGlobal name is not string, instruction pointer: " + instructionPointer.toString() + ".");
    }
    string name = object.toString();
    unsigned int index = globalsTable.find(name)->second;

    Pointer pointer = frameStack.global(index);

    operandStack.push(pointer);

    instructionPointer.bump();
}

void VirtualMachine::evalBranch()
{
    Pointer pointer = operandStack.top();
    operandStack.pop();

    bool doJump = true;

    if(heap[pointer].isNull())
    {
        doJump = false;
    }
    else if (heap[pointer].isBoolean() && heap[pointer].toBoolean() == false)
    {
        doJump = false;
    }

    if (! doJump)
    {
        instructionPointer.bump();
        return;
    }

    ConstantPoolIndex nameIndex(code(instructionPointer + 1),
                                code(instructionPointer + 2));

    ProgramObject object = constantPool(nameIndex);
    if (! object.isString())
    {
        throw Exception("Branch target is not string, instruction pointer: " + instructionPointer.toString() + ".");
    }
    string name = object.toString();

    instructionPointer = labelTable.find(name)->second;
}

void VirtualMachine::evalJump()
{
    ConstantPoolIndex nameIndex(code(instructionPointer + 1),
                                code(instructionPointer + 2));

    ProgramObject object = constantPool(nameIndex);
    if (! object.isString())
    {
        throw Exception("Jump target is not string, instruction pointer: " + instructionPointer.toString() + ".");
    }
    string name = object.toString();

    instructionPointer = labelTable.find(name)->second;
}

void VirtualMachine::evalReturn()
{
    instructionPointer = frameStack.getReturnAddress();
    frameStack.pop();
}

void VirtualMachine::evalDrop()
{
    if(operandStack.empty())
    {
        throw Exception("Drop from empty stack, instruction pointer: " + instructionPointer.toString() + ".");
    }
    operandStack.pop();

    instructionPointer.bump();

    //cerr << "drop" << endl;
}
