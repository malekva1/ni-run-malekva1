#ifndef HEAPLOGER_H
#define HEAPLOGER_H

#include <string>
#include <list>
#include <chrono>

extern bool doHeapLog;
extern std::string heapLogPath;

class HeapLoger
{
public:
    HeapLoger() = default;

    void addStart();
    void addAllocation(unsigned int size);
    void addGC(unsigned int size);

    void writeLog() const;
private:
    std::chrono::time_point<std::chrono::steady_clock> timeNow() const;

    std::chrono::time_point<std::chrono::steady_clock> startTime;

    enum EventType {start, allcation, gc};

    struct LogItem
    {
        unsigned int size;
        std::chrono::time_point<std::chrono::steady_clock> timestamp;
        EventType eventType;
    };

    std::list<LogItem> log;
};

#endif // HEAPLOGER_H
