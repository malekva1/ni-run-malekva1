#!/bin/bash

ABSOLUTE_PATH="$(realpath "$0")"
SCRIPT_PATH="$(dirname "$ABSOLUTE_PATH")"

if [ "$1" != "run" ]
then 
    >&2 echo "Unsuported command $1"
    exit 1
fi

INPUTFILE="$2" 

"$SCRIPT_PATH"/parser.run parse --format=json "$INPUTFILE" | "$SCRIPT_PATH"/interpreter.run
