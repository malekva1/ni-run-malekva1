#include "astprinter.h"

#include "ast.h"

#include <iostream>

using namespace std;

void AstPrinter::print(Ast * program)
{
    AstPrinter printer;
    program->accept(&printer);
}

AstPrinter::AstPrinter()
{
    spaces = 0;
}

void AstPrinter::visit(Integer * node)
{
    printSpaces();
    cout << node->value << endl;
}

void AstPrinter::visit(Boolean * node)
{
    printSpaces();
    cout << node->value << endl;
}

void AstPrinter::visit(Null * node)
{
    printSpaces();
    cout << "null" << endl;
}

void AstPrinter::visit(Variable * node)
{
    printSpaces();
    cout << node->name << " = " << endl;
    int spacesBackup = spaces;
    spaces = spaces + 2;
    node->value->accept(this);
    spaces = spacesBackup;
}

void AstPrinter::visit(AccessVariable * node)
{
    printSpaces();
    cout << node->name << endl;
}

void AstPrinter::visit(AssignVariable * node)
{
    printSpaces();
    cout << node->name << " <- " << endl;
    int spacesBackup = spaces;
    spaces = spaces + 2;
    node->value->accept(this);
    spaces = spacesBackup;
}

void AstPrinter::visit(Function * node)
{
    printSpaces();
    cout << "function" << node->name << endl;

    int spacesBackup = spaces;
    spaces = spaces + 2;
    for (const auto & element : node->parameters)
    {
        printSpaces();
        cout << "- " << element << endl;
    }

    printSpaces();
    cout << "-------" << endl;
    node->body->accept(this);
    printSpaces();
    cout << "-------" << endl;
    spaces = spacesBackup;
}

void AstPrinter::visit(CallFunction * node)
{
    printSpaces();
    cout << node->name << "()" << endl;
    int spacesBackup = spaces;
    spaces = spaces + 2;
    for (const auto & element : node->arguments)
    {
        element->accept(this);
    }
    spaces = spacesBackup;
}

void AstPrinter::visit(Print * node)
{
    printSpaces();
    cout << "print " << node->format << endl;
    int spacesBackup = spaces;
    spaces = spaces + 2;
    for (const auto & element : node->arguments)
    {
        element->accept(this);
    }
    spaces = spacesBackup;
}

void AstPrinter::visit(Block * node)
{
    printSpaces();
    cout << "BLOCK:" <<endl;
    int spacesBackup = spaces;
    spaces = spaces + 2;
    for (const auto & element : node->statements)
    {
        element->accept(this);
    }
    spaces = spacesBackup;
}

void AstPrinter::visit(Top * node)
{
    printSpaces();
    cout << "TOP:" <<endl;
    int spacesBackup = spaces;
    spaces = spaces + 2;
    for (const auto & element : node->statements)
    {
        element->accept(this);
    }
    spaces = spacesBackup;
}

void AstPrinter::visit(Loop * node)
{
    printSpaces();
    cout << "while" <<endl;

    int spacesBackup = spaces;
    spaces = spaces + 2;
    node->condition->accept(this);

    spaces = spacesBackup + 4;
    printSpaces();
    cout << "-------" << endl;
    node->body->accept(this);
    printSpaces();
    cout << "-------" << endl;
    spaces = spacesBackup;
}

void AstPrinter::visit(Conditional * node)
{
    printSpaces();
    cout << "if" <<endl;

    int spacesBackup = spaces;
    spaces = spaces + 2;
    node->condition->accept(this);

    spaces = spacesBackup + 4;
    printSpaces();
    cout << "-------" << endl;
    node->consequent->accept(this);
    printSpaces();
    cout << "-------" << endl;
    node->alternative->accept(this);
    printSpaces();
    cout << "-------" << endl;
    spaces = spacesBackup;
}

void AstPrinter::visit(Object * node)
{
    printSpaces();

}

void AstPrinter::visit(Array * node)
{
    printSpaces();
    cout << "array:" <<endl;

    int spacesBackup = spaces;
    spaces = spaces + 2;
    node->size->accept(this);
    node->value->accept(this);
    spaces = spacesBackup;
}

void AstPrinter::visit(AssignField * node)
{
    printSpaces();
}

void AstPrinter::visit(AssignArray * node)
{
    printSpaces();
    cout << "AssignArray:" <<endl;
    int spacesBackup = spaces;
    spaces = spaces + 2;
    node->array->accept(this);
    node->index->accept(this);
    node->value->accept(this);
    spaces = spacesBackup;
}

void AstPrinter::visit(AccessField * node)
{
    printSpaces();
}

void AstPrinter::visit(AccessArray * node)
{
    printSpaces();
    cout << "AccessArray:" <<endl;
    int spacesBackup = spaces;
    spaces = spaces + 2;
    node->array->accept(this);
    node->index->accept(this);
    spaces = spacesBackup;
}

void AstPrinter::visit(CallMethod * node)
{
    printSpaces();
}



void AstPrinter::printSpaces()
{
    for (int i = 0; i < spaces; i++)
    {
        cout << " ";
    }
}

