#ifndef INTERPRETER_H
#define INTERPRETER_H

#include "astvisitor.h"
#include <variant>
#include <map>
#include <vector>


class Interpreter : protected AstVisitor
{
public:
    static int run(Ast * program);

    void visit(Integer * node);
    void visit(Boolean * node);
    void visit(Null * node);
    void visit(Variable * node);
    void visit(AccessVariable * node);
    void visit(AssignVariable * node);
    void visit(Function * node);
    void visit(CallFunction * node);
    void visit(Print * node);
    void visit(Block * node);
    void visit(Top * node);
    void visit(Loop * node);
    void visit(Conditional * node);

    void visit(Object * node);
    void visit(Array * node);
    void visit(AssignField * node);
    void visit(AssignArray * node);
    void visit(AccessField * node);
    void visit(AccessArray * node);
    void visit(CallMethod * node);

protected:
    Interpreter();

    void evalIntOperator(int value, std::string operatorName, Ast * operant);
    void evalBoolOperator(bool value, std::string operatorName, Ast * operant);
    void evalNullOperator(std::string operatorName, Ast * operant);

    struct Func;
    struct Arr;
    struct Obj;

    using Value = std::variant<int, bool, void *, Func, Arr, Obj>;

    class Enviroment;

    struct Func
    {
        std::vector<std::string> parameters;
        Ast * body;
    };

    struct Arr
    {
        int length;
        Value * values;
    };

    struct Obj
    {
        Value * extends;
        Enviroment * members;
    };


    Value returnValue;
    static std::string printValue(Value value);

    class Enviroment
    {
    public:
        Enviroment * addLevel();
        void dropLevel();

        void addValue(std::string name, Value value);
        void setValue(std::string name, Value value);
        Value findValue(std::string name);

        std::string toString();

        struct NoValueException{};

        std::map<std::string, Value> values;
        Enviroment * next = nullptr;
    };

    Enviroment * enviroment;
    Enviroment globalEnviroment;
};

#endif // INTERPRETER_H
