#include "interpreter.h"

#include "ast.h"
#include <iostream>

using namespace std;

Interpreter::Interpreter()
{
    enviroment = &globalEnviroment;
}

int Interpreter::run(Ast * program)
{
    //cout << "run program" << endl;

    Interpreter interpreter;

    try
    {
        program->accept(&interpreter);
    }
    catch (int errorNum)
    {
        return errorNum;
    }
    return 0;
}

void Interpreter::evalIntOperator(int value, string operatorName, Ast * operant)
{
    operant->accept(this);
    if(returnValue.index() != 0)
    {
        if (operatorName == "==" || operatorName == "!=")
        {
            returnValue = operatorName == "!=";
            return;
        }
        else
        {
            cerr << "Argument of operator " << operatorName << " is not an iteger value." << endl;
            throw 1;
        }
    }
    int i = get<int>(returnValue);

    if (operatorName == "==") returnValue = value == i;
    else if (operatorName == "!=") returnValue = value != i;
    else if (operatorName == ">") returnValue = value > i;
    else if (operatorName == "<") returnValue = value < i;
    else if (operatorName == ">=") returnValue = value >= i;
    else if (operatorName == "<=") returnValue = value <= i;
    else if (operatorName == "+") returnValue = value + i;
    else if (operatorName == "-") returnValue = value - i;
    else if (operatorName == "/") returnValue = value / i;
    else if (operatorName == "*") returnValue = value * i;
    else if (operatorName == "%") returnValue = value % i;
    else
    {
        cerr << "Unsuported integer operator " << operatorName << "." << endl;
        throw 1;
    }
}

void Interpreter::evalBoolOperator(bool value, string operatorName, Ast * operant)
{
    operant->accept(this);
    if(returnValue.index() != 1)
    {
        if (operatorName == "==" || operatorName == "!=")
        {
            returnValue = operatorName == "!=";
            return;
        }
        else
        {
            cerr << "Argument of operator " << operatorName << " is not a boolean value." << endl;
            throw 1;
        }
    }

    bool i = get<bool>(returnValue);

    if (operatorName == "|") returnValue = value || i;
    else if (operatorName == "&") returnValue = value && i;
    else if (operatorName == "==") returnValue = value == i;
    else if (operatorName == "!=") returnValue = value != i;
    else
    {
        cerr << "Unsuported bool operator " << operatorName << "." << endl;
        throw 1;
    }

}

void Interpreter::evalNullOperator(std::string operatorName, Ast * operant)
{
    if (operatorName == "==" || operatorName == "!=")
    {
        operant->accept(this);
        bool isNull = returnValue.index() == 2;
        returnValue = isNull == (operatorName == "==");
    }
    else
    {
        cerr << "Unsuported null operator " << operatorName << "." << endl;
        throw 1;
    }
}

void Interpreter::visit(Integer * node)
{
    //cout << "run Integer" << endl;
    returnValue = node->value;
}

void Interpreter::visit(Boolean * node)
{
    //cout << "run Boolean" << endl;
    returnValue = node->value;
}

void Interpreter::visit(Null *)
{
    //cout << "run Null" << endl;
    returnValue = nullptr;
}

void Interpreter::visit(Variable * node)
{
    //cout << "run Variable" << endl;

    node->value->accept(this);
    enviroment->addValue(node->name, returnValue);
}

void Interpreter::visit(AccessVariable * node)
{
    //cout << "run AccessVariable " << node->name << endl;

    try
    {
        returnValue = enviroment->findValue(node->name);
    }
    catch (Enviroment::NoValueException)
    {
        cerr << "Access undefined variable " << node->name<< endl;
        throw 1;
    }
}

void Interpreter::visit(AssignVariable * node)
{
    //cout << "run AssignVariable" << endl;

    try
    {
        node->value->accept(this);
        enviroment->setValue(node->name, returnValue);
    }
    catch (Enviroment::NoValueException)
    {
        cerr << "Assign to undefined variable " << node->name<< endl;
        throw 2;
    }
}

void Interpreter::visit(Function * node)
{
    //cout << "run Function" << endl;

    Func f;
    f.parameters = node->parameters;
    f.body = node->body;
    enviroment->addValue(node->name, f);
}

void Interpreter::visit(CallFunction * node)
{
    //cout << "run CallFunction " << node->name << endl;
    try
    {
        Value function = enviroment->findValue(node->name);
        if (function.index() != 3)
        {
            cerr << node->name << "is not a function." << endl;
            throw 5;
        }

        Func f = get<Func>(function);

        if (node->arguments.size() != f.parameters.size())
        {
            cerr << "Number of arguments of function "<< node->name <<" differs from number of parameters." << endl;
            throw 6;
        }

        Enviroment * bodyEnviroment = globalEnviroment.addLevel();

        for (unsigned int i = 0; i < f.parameters.size(); i++)
        {
            node->arguments[i]->accept(this);
            bodyEnviroment->addValue(f.parameters[i], returnValue);
        }

        Enviroment * enviromentBackup = enviroment;
        enviroment = bodyEnviroment;
        f.body->accept(this);
        bodyEnviroment->dropLevel();

        enviroment = enviromentBackup;
    }
    catch (Enviroment::NoValueException)
    {
        cerr << "Undefined function " << node->name<< endl;
        throw 5;
    }
}

void Interpreter::visit(Print * node)
{
    //cout << "run Print" << endl;


    vector<string> arguments;
    for(auto & it : node->arguments)
    {
        it->accept(this);
        arguments.push_back(printValue(returnValue));
    }

    int printedArguments = 0;

    for (unsigned int i = 0; i < node->format.length(); i++)
    {
        char ch = node->format[i];
        if (ch == '\\')
        {
            if (i == node->format.length() - 1)
            {
                cerr << "\\ can not be last char in format string" << endl;
                throw 2;
            }

            char nextCh = node->format[i+1];

            if (nextCh == '~')
            {
                cout << "~";
            }
            else if (nextCh == 'n')
            {
                cout << endl;
            }
            else if (nextCh == '"')
            {
                cout << "\"";
            }
            else if (nextCh == 'r')
            {
                cout << "\r";
            }
            else if (nextCh == 't')
            {
                cout << "\t";
            }
            else if (nextCh == '\\')
            {
                cout << "\\";
            }
            else
            {
                cerr << "\\ can not be followed by \"" << nextCh <<"\"" << endl;
                throw 3;
            }
            i++;
            continue;
        }
        else if (ch == '~')
        {
            cout << arguments[printedArguments];
            printedArguments++;
        }
        else
        {
            cout << ch;
        }
    }
    returnValue = nullptr;
}

void Interpreter::visit(Block * node)
{
    //cout << "run Block" << endl;

    Enviroment * enviromentBackup = enviroment;
    enviroment = enviroment->addLevel();
    for (auto i : node->statements)
    {
        i->accept(this);
    }
    enviroment->dropLevel();
    enviroment = enviromentBackup;
}

void Interpreter::visit(Top * node)
{
    //cout << "run Top" << endl;

    for (auto i : node->statements)
    {
        i->accept(this);
    }
}

void Interpreter::visit(Loop * node)
{
    //cout << "run Loop" << endl;

    while (true)
    {
        node->condition->accept(this);

        if (returnValue.index() == 1) // bool
        {
            if (get<bool>(returnValue) == false)
            {
                returnValue = nullptr;
                break;
            }
        }
        else if (returnValue.index() == 2) // null
        {
            break;
        }

        node->body->accept(this);
    }
}

void Interpreter::visit(Conditional * node)
{
    //cout << "run Conditional" << endl;

    node->condition->accept(this);
    bool res = true;

    if (returnValue.index() == 1) // bool
    {
        res = get<bool>(returnValue);
    }
    else if (returnValue.index() == 2) // null
    {
        res = false;
    }

    if (res)
    {
        node->consequent->accept(this);
    }
    else
    {
        node->alternative->accept(this);
    }
}

void Interpreter::visit(Object * node)
{
    Obj res;
    node->extends->accept(this);
    res.extends = new Value(returnValue);

    Enviroment * enviromentBackup = enviroment;
    enviroment = enviroment->addLevel();

    for (auto it : node->members)
    {
        it->accept(this);
    }
    res.members = enviroment;
    res.members->next = nullptr;
    enviroment = enviromentBackup;
    returnValue = res;
}

void Interpreter::visit(Array * node)
{
    Arr res;
    node->size->accept(this);

    if (returnValue.index() != 0)
    {
        cerr << "Size of array must be number" << endl;
        throw 1;
    }

    res.length = get<int>(returnValue);

    res.values = new Value [res.length];

    for (int i = 0; i < res.length; i++)
    {
        node->value->accept(this);
        res.values[i] = returnValue;
    }

    returnValue = res;
}

void Interpreter::visit(AssignField * node)
{
    node->object->accept(this);

    if (returnValue.index() != 5)
    {
        cerr << "Assign Field on not an object." << endl;
        throw 1;
    }

    Obj object = get<Obj>(returnValue);
    node->value->accept(this);

    try
    {
        object.members->setValue(node->field, returnValue);
    }
    catch (Enviroment::NoValueException)
    {
        cerr << "Assign undefined field " << node->field << "." << endl;
        throw 1;
    }
}

void Interpreter::visit(AssignArray * node)
{
    node->array->accept(this);

    Value receiver = returnValue;
    while (receiver.index() == 5)
    {
        Obj object = get<Obj>(receiver);

        try
        {
            object.members->findValue("set");
            CallMethod cm;
            cm.name = "set";
            cm.object = node->array;
            cm.arguments.push_back(node->index);
            cm.arguments.push_back(node->value);
            cm.accept(this);
            return;
        }
        catch (Enviroment::NoValueException)
        {
            receiver = *object.extends;
            continue;
        }
    }

    if (receiver.index() != 4)
    {
        cerr << "Assign array on not an array." << endl;
        throw 1;
    }
    Arr array = get<Arr>(receiver);

    node->index->accept(this);
    if (returnValue.index() != 0)
    {
        cerr << "Index in assign array is not a number." << endl;
        throw 1;
    }
    int index = get<int>(returnValue);

    node->value->accept(this);
    array.values[index] = returnValue;
}

void Interpreter::visit(AccessField * node)
{
    node->object->accept(this);

    if (returnValue.index() != 5)
    {
        cerr << "Access Field on not an object." << endl;
        throw 1;
    }

    Obj object = get<Obj>(returnValue);

    try
    {
        returnValue = object.members->findValue(node->field);
    }
    catch (Enviroment::NoValueException)
    {
        cerr << "Access undefined field " << node->field << "." << endl;
        throw 1;
    }
}

void Interpreter::visit(AccessArray * node)
{

    node->array->accept(this);

    Value receiver = returnValue;
    while (receiver.index() == 5)
    {
        Obj object = get<Obj>(receiver);

        try
        {
            object.members->findValue("get");
            CallMethod cm;
            cm.name = "get";
            cm.object = node->array;
            cm.arguments.push_back(node->index);
            cm.accept(this);
            return;
        }
        catch (Enviroment::NoValueException)
        {
            receiver = *object.extends;
            continue;
        }
    }

    if (receiver.index() != 4)
    {
        cerr << "Access array on not an array." << endl;
        throw 1;
    }
    Arr array = get<Arr>(receiver);


    node->index->accept(this);
    if (returnValue.index() != 0)
    {
        cerr << "Index in access array is not a number." << endl;
        throw 1;
    }
    int index = get<int>(returnValue);

    returnValue = array.values[index];
}

void Interpreter::visit(CallMethod * node)
{
    //cout << "run CallMethod " << node->name << endl;

    node->object->accept(this);

    Value receiver = returnValue;

    Value method = nullptr;
    while (receiver.index() == 5)
    {
        Obj object = get<Obj>(receiver);

        try
        {
            method = object.members->findValue(node->name);
            break;
        }
        catch (Enviroment::NoValueException)
        {
            receiver = *object.extends;
            continue;
        }
    }
    if (receiver.index() == 5) // is object
    {
        if (method.index() != 3)
        {
            cerr << node->name << " is not a method." << endl;
            throw 5;
        }

        Func f = get<Func>(method);

        if (node->arguments.size() != f.parameters.size())
        {
            cerr << "Number of arguments of method "<< node->name <<" differs from number of parameters." << endl;
            throw 6;
        }

        Enviroment * bodyEnviroment = globalEnviroment.addLevel();

        bodyEnviroment->addValue("this", returnValue);

        for (unsigned int i = 0; i < f.parameters.size(); i++)
        {
            node->arguments[i]->accept(this);
            bodyEnviroment->addValue(f.parameters[i], returnValue);
        }

        Enviroment * enviromentBackup = enviroment;
        enviroment = bodyEnviroment;

        f.body->accept(this);

        enviroment->dropLevel();
        enviroment = enviromentBackup;
        return;
    }
    else if (receiver.index() < 3) // is int, bool or null
    {
        if(node->arguments.size() != 1)
        {
            cerr << "Invalid number of arguments for method " << node->name << "." << endl;
            throw 1;
        }

        if (receiver.index() == 0)
        {
            evalIntOperator(get<int>(receiver), node->name, node->arguments[0]);
        }
        else if(receiver.index() == 1)
        {
            evalBoolOperator(get<bool>(receiver), node->name, node->arguments[0]);
        }
        else if(receiver.index() == 2)
        {
            evalNullOperator(node->name, node->arguments[0]);
        }
        return;
    }
    else if (receiver.index() == 4) // is array
    {
        Arr array = get<Arr>(receiver);

        if (node->name == "set")
        {
            if(node->arguments.size() != 2)
            {
                cerr << "Invalid number of arguments for method " << node->name << "." << endl;
                throw 1;
            }

            node->arguments[0]->accept(this);
            if(returnValue.index() != 0)
            {
                cerr << "Argument of method set is not an integer value." << endl;
                throw 1;
            }
            int i = get<int>(returnValue);
            if (i >= array.length)
            {
                cerr << "Argument of method set is greater then lenght of array." << endl;
                throw 1;
            }
            node->arguments[1]->accept(this);

            array.values[i] = returnValue;

            return;
        }
        else if (node->name == "get")
        {
            if(node->arguments.size() != 1)
            {
                cerr << "Invalid number of arguments for method " << node->name << "." << endl;
                throw 1;
            }

            node->arguments[0]->accept(this);
            if(returnValue.index() != 0)
            {
                cerr << "Argument of method get is not an integer value." << endl;
                throw 1;
            }

            int i = get<int>(returnValue);

            if (i >= array.length)
            {
                cerr << "Argument of method set is greater then lenght of array." << endl;
                throw 1;
            }

            returnValue = array.values[i];
            return;
        }
        else
        {
            cerr << "Invalid array method " << node->name << "." << endl;
            throw 1;
        }
    }
    else
    {
        cerr << "Cannot call method on function." << endl;
        throw 1;
    }
}

string Interpreter::printValue(Value value)
{
    if (value.index() == 0)
    {
        return to_string(get<int>(value));
    }
    else if (value.index() == 1)
    {
        return get<bool>(value) ? "true" : "false";
    }
    else if (value.index() == 2)
    {
        return "null";
    }
    else if (value.index() == 3)
    {
        return "function";
    }
    else if (value.index() == 4)
    {
        string res = "[";
        Arr array = get<Arr>(value);
        if (array.length != 0)
        {
            res += printValue(array.values[0]);
        }
        for (int i = 1; i < array.length; i++)
        {
            res += ", " + printValue(array.values[i]);
        }
        return res + "]";
    }
    else
    {
        Obj object = get<Obj>(value);

        string extends = "";
        if (object.extends->index() != 2)
        {
            extends = "..=" + printValue(*object.extends);
        }
        string members = object.members->toString();

        string res = "object(";
        res += extends;
        if (extends != "" && members != "")
        {
            res += ", ";
        }
        res += members;

        return res + ")";
    }
}

Interpreter::Enviroment * Interpreter::Enviroment::addLevel()
{
    Enviroment * newEnviroment = new Enviroment;
    newEnviroment->next = this;
    return newEnviroment;
}

void Interpreter::Enviroment::dropLevel()
{
    delete this;
    /*
    if (next != nullptr)
    {
        values = next->values;
        Enviroment * tmp = next;
        next = next->next;
        delete tmp;
    }
    */
}

void Interpreter::Enviroment::addValue(string name, Value value)
{
    values.insert(make_pair(name, value));
}

void Interpreter::Enviroment::setValue(std::string name, Value value)
{
    Enviroment * current = this;
    do
    {
        if (current->values.find(name)!= current->values.end())
        {
            current->values.at(name) = value;
            return;
        }

        current = current->next;
    }
    while (current != nullptr);
    throw NoValueException();
}

Interpreter::Value Interpreter::Enviroment::findValue(string name)
{
    Enviroment * current = this;
    do
    {
        if (current->values.find(name)!= current->values.end())
        {
            return current->values.at(name);
        }

        current = current->next;
    }
    while (current != nullptr);
    throw NoValueException();
}

string Interpreter::Enviroment::toString()
{
    if (values.empty())
    {
        return "";
    }
    string result = "";
    bool first = true;


    for (map<string, Value>::reverse_iterator it = values.rbegin(); it != values.rend(); ++it)
    {
        if (it->second.index() != 3)
        {
            if (first)
            {
                first = false;
            }
            else
            {
                result += ", ";
            }
            result += it->first + "=" + printValue(it->second);
        }
    }
    return result;
}
