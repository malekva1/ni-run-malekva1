#ifndef AST_H
#define AST_H

#include <string>
#include <vector>

class AstVisitor;

struct Ast{
    virtual void accept(AstVisitor * visitor) = 0;
};

struct Integer : public Ast
{
    int value;
    void accept(AstVisitor * visitor);
};

struct Boolean : public Ast
{
    bool value;
    void accept(AstVisitor * visitor);
};

struct Null : public Ast
{
    void accept(AstVisitor * visitor);
};

struct Variable : public Ast
{
    std::string name;
    Ast * value;
    void accept(AstVisitor * visitor);
};

struct AccessVariable : public Ast
{
    std::string name;
    void accept(AstVisitor * visitor);
};

struct AssignVariable : public Ast
{
    std::string name;
    Ast * value;
    void accept(AstVisitor * visitor);
};

struct Function : public Ast
{
    std::string name;
    std::vector<std::string> parameters;
    Ast * body;
    void accept(AstVisitor * visitor);
};

struct CallFunction : public Ast
{
    std::string name;
    std::vector<Ast *> arguments;
    void accept(AstVisitor * visitor);
};

struct Print : public Ast
{
    std::string format;
    std::vector<Ast *> arguments;
    void accept(AstVisitor * visitor);
};

struct Block : public Ast
{
    std::vector<Ast *> statements;
    void accept(AstVisitor * visitor);
};

struct Top : public Ast
{
    std::vector<Ast *> statements;
    void accept(AstVisitor * visitor);
};

struct Loop : public Ast
{
    Ast * condition;
    Ast * body;
    void accept(AstVisitor * visitor);
};

struct Conditional : public Ast
{
    Ast * condition;
    Ast * consequent;
    Ast * alternative;
    void accept(AstVisitor * visitor);
};

struct Object : public Ast
{
    Ast * extends;
    std::vector<Ast *> members;
    void accept(AstVisitor * visitor);
};

struct Array : public Ast
{
    Ast * size;
    Ast * value;
    void accept(AstVisitor * visitor);
};

struct AssignField : public Ast
{
    Ast * object;
    std::string field;
    Ast * value;
    void accept(AstVisitor * visitor);
};

struct AssignArray : public Ast
{
    Ast * array;
    Ast * index;
    Ast * value;
    void accept(AstVisitor * visitor);
};

struct AccessField : public Ast
{
    Ast * object;
    std::string field;
    void accept(AstVisitor * visitor);
};

struct AccessArray : public Ast
{
    Ast * array;
    Ast * index;
    void accept(AstVisitor * visitor);
};

struct CallMethod : public Ast
{
    Ast * object;
    std::string name;
    std::vector<Ast *> arguments;
    void accept(AstVisitor * visitor);
};

#endif // AST_H
