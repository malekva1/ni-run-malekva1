Files and dirs:
- parser.run - compiled rust runtime and parser, should run on Ubuntu
- interpreter.run - compiled my C++ interpreter, should run on Ubuntu
- fml.sh - script, uses parser and interpreter to execute fml programs
- source - directory with C++ source of my interpreter
- build.sh - script, compiles C++ source files to interpreter.run
